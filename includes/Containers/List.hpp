#ifndef LIST_HPP
#define LIST_HPP

#include "../../srcs/main.hpp"

namespace ft
{
    template<typename T>
    bool Compare(const T &a, const T &b) { return (a < b); }

    template<class T, class Alloc = std::allocator<T> >
    class list
    {
    public:
        /* Aliases definition */
        typedef T                                               value_type;
        typedef Alloc                                           allocator_type;

        typedef value_type&                                     reference;
        typedef value_type*                                     pointer;

        typedef Node<T>*                                        node_pointer;
        typedef Node<T>                                         node;

        typedef const value_type&                               const_reference;
        typedef const value_type*                               const_pointer;

        typedef Bidirectional<value_type, false>                iterator;
        typedef Bidirectional<value_type, true>                 const_iterator;

        typedef RevBidirectional<value_type, false>             reverse_iterator;
        typedef RevBidirectional<value_type, true>              const_reverse_iterator;

        typedef long int                                        difference_type;
        typedef size_t                                          size_type;
    
    private:
        /* Attributs */
        node_pointer        _head;          // first node 
        node_pointer        _tail;          // last node
        size_type           _size;          // size of list 
        allocator_type      _alloc;         // copy of allocator_type object.

    public:
        /* Constructors */
        explicit list (const allocator_type& alloc = allocator_type());
        explicit list (size_type n, const value_type& val = value_type(), const allocator_type& alloc = allocator_type());
        list (iterator first, iterator last, const allocator_type& alloc = allocator_type());
        list (const_iterator first, const_iterator last, const allocator_type& alloc = allocator_type());
        list(const list& x);
        /* Destructor */
        ~list();
        /* Overloading operator */
        list& operator = (const list& assignObj);
    
    public:
        /* Iterators */
        const_iterator cbegin() const { return const_iterator(this->_head->next); }
        const_iterator cend() const { return const_iterator(this->_tail); }
        iterator begin() { return iterator(this->_head->next); }
        iterator end() { return iterator(this->_tail); }

        const_reverse_iterator crbegin() const { return const_reverse_iterator(this->_tail->prev); }
        const_reverse_iterator crend() const { return const_reverse_iterator(this->_head); }
        reverse_iterator rbegin() { return reverse_iterator(this->_tail->prev); }
        reverse_iterator rend() { return reverse_iterator(this->_head); }

    public:
        /* Capacity */
        bool empty() const { return (this->_size == 0); }
        size_type size() const { return (this->_size); }
		size_type max_size() const	{ return (std::numeric_limits<size_type>::max() / (sizeof(node))); }

    public:
        /* Element access */
        reference front() { return (this->_head->next->data); }
        const_reference front() const { return (this->_head->next->data); }
        reference back() { return (this->_tail->prev->data); }
        const_reference back() const { return (this->_tail->prev->data); }
    
    public:
        /* Modifiers */
        void assign (size_type n, const value_type& val);
        void assign (iterator first, iterator last);
        void assign (const_iterator first, const_iterator last);
        void push_front (const value_type& val);
        void pop_front();
        void push_back (const value_type& val);
        void pop_back();
        iterator insert (iterator position, const value_type& val);
        void insert (iterator position, size_type n, const value_type& val);
        void insert (iterator position, iterator first, iterator last);
        void insert (iterator position, const_iterator first, const_iterator last);
        iterator erase (iterator position);
        iterator erase (iterator first, iterator last);
        void swap (list& x);
        void resize (size_type n, value_type val = value_type());
        void clear();

    public:
        /* Operations */
        void splice (iterator position, list& x);
        void splice (iterator position, list& x, iterator i);
        void splice (iterator position, list& x, iterator first, iterator last);
        void remove (const value_type& val);
        
        void unique();
        void merge (list& x);
        void sort();
        void reverse();

        template <class Predicate>
        void remove_if (Predicate pred)
        {
            iterator it = this->begin();
            iterator ite = this->end();

            for (;it != ite ;)
            {
                if (pred(*it) == true)
                    it = this->erase(it);
                else 
                    ++it;
            }
        }

        /*
            Prend comme argument une fonction de comparaison spécifique qui détermine
            l'"unicité" d'un élément. En fait, n'importe quel comportement peut être
            implémenté (et pas seulement une comparaison d'égalité.
        */
        template <class BinaryPredicate>
        void unique (BinaryPredicate binary_pred)
        {
            iterator it = this->begin();
            iterator ite = this->end();

            ++it;
            for (; it != ite ;)
            {
                if (binary_pred(*it, it.getCurrent()->prev->data))
                    it = this->erase(it);
                else 
                    ++it;
            }
        }

        /*
        Merge
        Fusionne x dans la liste en transférant tous ses éléments à leurs positions 
        ordonnées respectives dans le conteneur (les deux conteneurs doivent déjà être ordonnés).
        */
        template <class Compare>
        void merge (list& x, Compare comp)
        {
		    iterator it = begin();
			iterator ite = end();
			iterator x_it = x.begin();
			iterator x_ite = x.end();

			if (&x == this)
				return ;
			while (it != ite && x_it != x_ite)
			{
				while (it != ite && !comp(*x_it, *it))
					++it;
				++x_it;
				this->splice(it, x, x_it.getCurrent()->prev);
			}

			if (x_it != x_ite)
				this->splice(it, x, x_it, x_ite);
		}


        template <class Compare>
        void sort (Compare comp)
        {
            iterator it = this->begin();
            iterator index = this->begin();
            iterator ite = this->end();
            T swap;

            while (++index != ite )
            {
                if (comp(*index, *it))
                {
                    swap = *it;
                    *it = *index;
                    *index = swap;

                    it = this->begin();
                    index = this->begin();
                }
                else 
                    ++it;
            }
        }


    private:
        /* Methods */
        void default_list_initialisation(const allocator_type& alloc)
        {
            this->_alloc = alloc;
            this->_size = 0;
            this->_head = new node();
            this->_tail = new node();
            this->_head->next = this->_tail;    // connect head node to tail node
            this->_tail->prev = this->_head;    // connect tail node to head node
        }

        size_t nodeCounter(iterator first, iterator last)
        {
            size_t i = 0;
            for (; first != last ; ++first)
                i++;
            return (i);
        }

    }; // class list

     /***** Class implementation *****/

    /*** Member functions ***/

    /* Constructors */
    template <class T, class Alloc>
    list<T, Alloc>::list (const allocator_type& alloc)
    { 
        this->default_list_initialisation(alloc);
    }

    template <class T, class Alloc>
    list<T, Alloc>::list (size_type n, const value_type& val, const allocator_type& alloc)
    {
        this->default_list_initialisation(alloc);
        this->assign(n, val);
    }

    template <class T, class Alloc>
    list<T, Alloc>::list (iterator first, iterator last, const allocator_type& alloc)
    {
        this->default_list_initialisation(alloc);
        this->assign(first, last);
    }

    template <class T, class Alloc>
    list<T, Alloc>::list (const_iterator first, const_iterator last, const allocator_type& alloc)
    {
        this->default_list_initialisation(alloc);
        this->assign(first, last);
    }

    template <class T, class Alloc>
    list<T, Alloc>::list(const list& x)
    {
        this->default_list_initialisation(x._alloc);
        this->assign(x.cbegin(), x.cend());
    }

    template <class T, class Alloc>
    list<T, Alloc>::~list()
    {
        this->clear();
        delete this->_tail;
        delete this->_head;
    }

    /* Overloading operator = */
    template <class T, class Alloc>
    list<T, Alloc>& list<T, Alloc>::operator = (const list<T, Alloc>& assignObj)
    {
        this->clear();
        delete this->_head->next;
	    delete this->_tail->prev;
        this->default_list_initialisation(assignObj._alloc);
        this->assign(assignObj.cbegin(), assignObj.cend());
        return (*this);
    }

    /* Modifiers */
    template <class T, class Alloc>
    void  list<T, Alloc>::assign (size_type n, const value_type& val)
    {
        this->clear();
        for (size_type i = 0; i < n ; i++)
            this->push_back(val);
    }

    template <class T, class Alloc>
    void list<T, Alloc>::assign (iterator first, iterator last)
    {
        this->clear();
        for (; first != last ; ++first)
            this->push_back(*first);
    }

    template <class T, class Alloc>
    void list<T, Alloc>::assign (const_iterator first, const_iterator last)
    {
        this->clear();
        for (; first != last ; ++first)
            this->push_back(*first);
    }

    template < class T, class Alloc >
    void list<T, Alloc>::push_front (const value_type& val)
    {
        node_pointer New = new node(val);
        New->next = this->_head->next;;
        New->prev = this->_head;
        this->_head->next->prev = New;  // connect the prev pointer from head->next node to the new node
        this->_head->next = New;        // connect the next pointer from head node to the new node
        this->_size++;
    }

    template < class T, class Alloc >
    void list<T, Alloc>::pop_front()
    {
        if (this->_size)
        {   
            node_pointer tmp = this->_head->next->next; 
            tmp->prev = this->_head;
            delete this->_head->next;
            this->_head->next = tmp;
            --this->_size;
        }
    }

    template < class T, class Alloc >
    void list<T, Alloc>::push_back (const value_type& val)
    {
        node_pointer New = new node(val);
        New->next = this->_tail;
        New->prev = this->_tail->prev;

        this->_tail->prev->next = New; // connect the next pointer from previous node to the new node
        this->_tail->prev = New;
        this->_size++;
    }

    template < class T, class Alloc >
    void list<T, Alloc>::pop_back()
    {
        if (this->_size)
        {    
            node_pointer tmp = this->_tail->prev->prev; 
            tmp->next = this->_tail;
            delete this->_tail->prev;
            this->_tail->prev = tmp;
            --this->_size;
        }
    }

    template < class T, class Alloc >
    typename list<T, Alloc>::iterator list<T, Alloc>::insert (iterator position, const value_type& val)
    {
        node_pointer New = new node(val);
        New->prev = position.getCurrent()->prev;
        New->next = position.getCurrent();
        position.getCurrent()->prev->next = New;
        position.getCurrent()->prev = New;
        this->_size++;
        return (iterator(New)); 
    }

    template < class T, class Alloc >
    void list<T, Alloc>::insert (iterator position, size_type n, const value_type& val)
    {
        while (n--)
            this->insert(position, val);
    }

    template < class T, class Alloc >
    void list<T, Alloc>::insert (iterator position, iterator first, iterator last)
    {
        for (; first != last; ++first)
            insert(position, *first);
    }
    
    template < class T, class Alloc >
    void list<T, Alloc>::insert (iterator position, const_iterator first, const_iterator last)
    {
        for (; first != last; ++first)
            insert(position, *first);
    }

    template < class T, class Alloc >
    typename list<T, Alloc>::iterator list<T, Alloc>::erase (iterator position)
    {
        node_pointer tmp = position.getCurrent();
        tmp->prev->next = tmp->next;
        tmp->next->prev = tmp->prev;
        position++;
        delete tmp;
        this->_size--;
        return (position);
    }  

    template < class T, class Alloc >
    typename list<T, Alloc>::iterator list<T, Alloc>::erase (iterator first, iterator last)
    {
        while (first != last)
            first = this->erase(first);
        return (first);
    }

    template < class T, class Alloc >
    void list<T, Alloc>::swap (list& x)
    {
        ft::list<T> tmp = x;
        x = *this;
        *this = tmp;
    }

    template < class T, class Alloc >
    void list<T, Alloc>::resize (size_type n, value_type val)
    {
        while (this->_size > n)
            this->pop_back();
        while (this->_size < n)
            this->push_back(val);
    }

    template < class T, class Alloc >
    void list<T, Alloc>::clear ()
    {
        while (this->_size)
            pop_back();
    }

    /* Operations */

    /*
        Splice:
        Transfère les éléments de x dans le conteneur, en les insérant à la position.
    */
    template < class T, class Alloc >
    void list<T, Alloc>::splice (iterator position, list& x)
    {
        while (x.size())
            splice(position, x, x.begin());
    }
    
    template < class T, class Alloc >
    void list<T, Alloc>::splice (iterator position, list& x, iterator i)
    {
        node_pointer tmp = i.getCurrent();
        node_pointer pos = position.getCurrent();
                
        // Linking previous and next node in x together
        tmp->next->prev = tmp->prev;
        tmp->prev->next = tmp->next;

        // Linking i in this list
        tmp->prev = pos->prev;
        tmp->next = pos;
        pos->prev->next = tmp;
        pos->prev = tmp;
                
        --x._size;
        ++this->_size;

    }

    template < class T, class Alloc >
    void list<T, Alloc>::splice (iterator position, list& x, iterator first, iterator last)
    {
        if (first == last)
            return;

        if (first.getCurrent()->next == last.getCurrent())
            splice(position, x, first);
        else
        {
            node_pointer pos = position.getCurrent();
            node_pointer x_first = first.getCurrent();
            node_pointer x_last = last.getCurrent();

            // Saving last range's elem
            node_pointer x_last_prev = x_last->prev;

            // Counting range' size
            size_t count = nodeCounter(first, last);
                    
            // Removing range from list x
            x_first->prev->next = x_last;
            x_last->prev = x_first->prev;

            // Linking range in this list
            x_first->prev = pos->prev;
            x_last_prev->next = pos;
            pos->prev->next = x_first;
            pos->prev = x_last_prev;
                    
            x._size -= count;
            this->_size += count;
        }
    }

    template < class T, class Alloc >
    void list<T, Alloc>::remove (const value_type& val)
    {
        iterator it = this->begin();
        iterator ite = this->end();

        for (; it != ite ;)
        {
            if (*it == val)
                it = this->erase(it);
            else 
                ++it;
        }
    }

    /*
        Unique :
        Supprime tous les éléments, sauf le premier, de chaque groupe consécutif 
        d'éléments égaux dans le conteneur.
    */

    template < class T, class Alloc >
    void list<T, Alloc>::unique ()
    {
        iterator it = this->begin();
        iterator ite = this->end();

        for (; it != ite ;)
        {
            if (*it == it.getCurrent()->prev->data)
                it = this->erase(it);
            else 
                ++it;
        }
    }

    template < class T, class Alloc >
    void list<T, Alloc>::merge (list& x)
    {
    	this->merge(x, Compare<T>);
    }

    template<typename T, typename Alloc >
    void list<T, Alloc>::sort()
    {
        this->sort(Compare<T>);
    }

    template<typename T, typename Alloc >
    void list<T, Alloc>::reverse()
    {
        list<T, Alloc> tmp;
        iterator it = this->begin();
        iterator ite = this->end();

        while (it != ite)
        {
            tmp.push_front(*it);
            ++it;
        }
        *this = tmp;
    }

    template<typename T, typename Alloc>
    bool operator == (const list<T, Alloc>& lhs, const list<T, Alloc>& rhs)
    {
        if (lhs.size() != rhs.size())
        {
            return false;
        }

        typename ft::list<T, Alloc>::const_iterator it_lhs = lhs.cbegin();
        typename ft::list<T, Alloc>::const_iterator ite_lhs = lhs.cend();
        typename ft::list<T, Alloc>::const_iterator it_rhs = rhs.cbegin();
        typename ft::list<T, Alloc>::const_iterator ite_rhs = rhs.cend();


        for (; (it_lhs != ite_lhs) && (it_rhs != ite_rhs) ; ++it_lhs, ++it_rhs)
        {
            if (*(it_lhs) != *(it_rhs))
                return (false);
        }

        return (true);
    }


    template<typename T, typename Alloc>
    bool operator != (const list<T,Alloc>& lhs, const list<T,Alloc>& rhs)
    {
        if (lhs == rhs)
            return (false);
        return (true);
    }

    template<typename T, typename Alloc>
    bool operator < (const list<T,Alloc>& lhs, const list<T,Alloc>& rhs)
    {
        typename ft::list<T, Alloc>::const_iterator first_lhs = lhs.cbegin();
        typename ft::list<T, Alloc>::const_iterator last_lhs = lhs.cend();
        typename ft::list<T, Alloc>::const_iterator first_rhs = rhs.cbegin();
        typename ft::list<T, Alloc>::const_iterator last_rhs = rhs.cend();

        while (first_lhs != last_lhs)
        {
            if (first_rhs == last_rhs || *first_rhs < *first_lhs)
                return (false);
            else if (*first_lhs < *first_rhs)
                return (true);
            ++first_lhs;
            ++first_rhs;
        }
        return (first_rhs != last_rhs);
    }

    template<typename T, typename Alloc>
    bool operator <= (const list<T,Alloc>& lhs, const list<T,Alloc>& rhs)
    {
        return !(rhs < lhs);
    }

    template<typename T, typename Alloc>
    bool operator > (const list<T,Alloc>& lhs, const list<T,Alloc>& rhs)
    {
        return (rhs < lhs);
    }

    template<typename T, typename Alloc>
    bool operator >= (const list<T,Alloc>& lhs, const list<T,Alloc>& rhs)
    {
        return !(lhs < rhs);
    }

    template<typename T, typename Alloc>
    void swap(list<T,Alloc>& x, list<T,Alloc>& y)
    {
        list<T,Alloc> temp = x;
        x = y;
        y = temp;
    }

} // namespace ft

#endif
