#ifndef MAP_ITERATOR_HPP
#define MAP_ITERATOR_HPP

#include "../../srcs/main.hpp"

    template <class Key, class T>
    struct b_node
    {
        std::pair<Key, T>           content;

        b_node*                     parent;
        b_node*                     left;
        b_node*                     right;
        bool                        end;
    };  // struct b_node

    template <typename K, typename T, typename Pointer, typename Reference>
	class map_iterator
	{
		public:
			typedef std::pair<K, T>                             value_type;
			typedef value_type&                                 reference;
			typedef b_node<K, T>*                               node_pointer;
			typedef map_iterator<K, T, Pointer, Reference>		curr_class;
        	typedef map_iterator<K, T, T*, T&>                  iterator;

			node_pointer                                        _current;

		private:
			node_pointer _next_node(node_pointer _current)
			{
				node_pointer next;
				if (!_current->right)
				{
					next = _current;
					while (next->parent && next == next->parent->right)
						next = next->parent;
					next = next->parent;
				}
				else
				{
					next = _current->right;
					while (next->left)
						next = next->left;
				}
				return (next);
			};

			node_pointer _prev_node(node_pointer _current)
			{
				node_pointer next;

				if (!_current->left)
				{
					next = _current;
					while (next->parent && next == next->parent->left)
						next = next->parent;
					next = next->parent;
				}
				else
				{
					next = _current->left;
					while (next->right)
						next = next->right;
				}
				return (next);
			};

		public:
			// map_iterator(void): __current(0) {}
			map_iterator(const node_pointer _current = 0): _current(_current) {}
			map_iterator(const iterator &other) { *this = other;}
			
			map_iterator &operator=(const iterator &other)
			{
				_current = other._current;
				return (*this);
			};
			
			node_pointer node(void) { return (_current);}
			
			value_type& operator*(void) { return (_current->content);}
			
			value_type* operator->(void) { return (&_current->content);}
			
			bool operator == (const curr_class &other) { return (_current == other._current);}
			bool operator != (const curr_class &other) { return (!(*this == other));}
			
			map_iterator& operator++(void)
			{
				_current = _next_node(_current);
				return (*this);
			}

			map_iterator& operator--(void)
			{
				_current = _prev_node(_current);
				return (*this);
			}

			map_iterator operator++(int)
			{
				map_iterator tmp(*this);
				this->operator++();
				return (tmp);
			}

			map_iterator operator--(int)
			{
				map_iterator tmp(*this);
				this->operator--();
				return (tmp);
			}

	};

    template <typename K, typename T, typename Pointer, typename Reference>
	class reverse_map_iterator
	{
		public:
			typedef std::pair<K, T>                                 value_type;
			typedef value_type&                                     reference;
			typedef b_node<K, T>*                                   node_pointer;
			typedef reverse_map_iterator<K, T, Pointer, Reference>	curr_class;
        	typedef reverse_map_iterator<K, T, T*, T&>              iterator;

			node_pointer                                            _current;

		private:
			node_pointer _next_node(node_pointer _current)
			{
				node_pointer next;
				if (!_current->right)
				{
					next = _current;
					while (next->parent && next == next->parent->right)
						next = next->parent;
					next = next->parent;
				}
				else
				{
					next = _current->right;
					while (next->left)
						next = next->left;
				}
				return (next);
			};

			node_pointer _prev_node(node_pointer _current)
			{
				node_pointer next;

				if (!_current->left)
				{
					next = _current;
					while (next->parent && next == next->parent->left)
						next = next->parent;
					next = next->parent;
				}
				else
				{
					next = _current->left;
					while (next->right)
						next = next->right;
				}
				return (next);
			};

		public:
			// reverse_map_iterator(void): __current(0) {}
			reverse_map_iterator(const node_pointer _current = 0): _current(_current) {}
			reverse_map_iterator(const iterator &other) { *this = other;}
			
			reverse_map_iterator &operator=(const iterator &other)
			{
				_current = other._current;
				return (*this);
			};
			
			node_pointer node(void) { return (_current);}
			
			value_type& operator*(void) { return (_current->content);}
			
			value_type* operator->(void) { return (&_current->content);}
			
			bool operator == (const curr_class &other) { return (_current == other._current);}
			bool operator != (const curr_class &other) { return (!(*this == other));}
			
			reverse_map_iterator& operator++(void)
			{
				_current = _prev_node(_current);
				return (*this);
			}

			reverse_map_iterator& operator--(void)
			{
				_current = _next_node(_current);
				return (*this);
			}

			reverse_map_iterator operator++(int)
			{
				reverse_map_iterator tmp(*this);
				this->operator++();
				return (tmp);
			}

			reverse_map_iterator operator--(int)
			{
				reverse_map_iterator tmp(*this);
				this->operator--();
				return (tmp);
			}
	};

#endif
