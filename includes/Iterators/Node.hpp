#ifndef NODE_HPP
#define NODE_HPP

#include "../../srcs/main.hpp"

template <typename T>
class Node
{
public:
    /* Aliases definition */
    typedef T       value_type;
    typedef Node*   pointer;
    /* Attributs */
    value_type       data;
    pointer          next;
    pointer          prev;
    /* Constructor */
    Node(const value_type& input = value_type()) : data(input), next(NULL), prev(NULL) {}

    /* Copy constructor */
    Node(const Node& other) { *this = (other); }
    /* Destructor */
    ~Node() {}
    /* Overloading operators */
    const Node& operator = (const Node& assignObj)
    {
        if (*this != &assignObj)
        {
            this->data = assignObj.data;
            this->next = assignObj.next;
            this->prev = assignObj.prev;
        }
    }

    bool operator == (const Node& other) const { return (this->_data == other._data && this->next == other.next && this->prev == this->prev); }
    bool operator != (const Node& other) const { return (*this != other); }

}; // class Node






// template<typename T>
// void    print_list(Node<T>* node)
// {
//     while (node != NULL)
//     {
//         std::cout << node->value << std::endl;
//         node = node->next;
//     }
// }

#endif
