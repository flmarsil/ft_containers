#ifndef ALLOCATOR_HPP
#define ALLOCATOR_HPP

#include "../../srcs/main.hpp"

namespace ft 
{
    template <class T>
    class allocator
    {
    public:
    /* Aliases definition */
        typedef T                   value_type;

        typedef value_type*         pointer;
        typedef value_type&         reference;
        typedef const value_type*   const_pointer;
        typedef const value_type&   const_reference;

        typedef size_t              size_type;
        typedef long int            difference_type;

    public:
    /* Constructors */
        allocator() throw() {};
        allocator (const allocator&) throw() {};
    /* Copy constructor */
    template <class U>
        allocator (const allocator<U>&) throw() {};
    /* Destructor */
        ~allocator() throw() {};
    /* Methods */
        pointer address (reference x) const { return (&x); }                    // return the adress of object x;
        const_pointer address (const_reference x) const { return (&x); }        // return the adress of object x;

        pointer allocate (size_type n)                                          // allocate memory block of storage - return a pointer to the new allocated area - not call the constructor
        {
            pointer ret;

            try
            {
                ret = reinterpret_cast<pointer>(::operator new(n * sizeof(value_type)));
            }
            catch(const std::exception& e)
            {
                std::cerr << e.what() << '\n';
            }
            
            return ret;

            // return (reinterpret_cast<pointer>(::operator new (n * sizeof(value_type))));
        }

        void deallocate (pointer p, size_type)                                  // release block of storage but does not call destructors
        {
            ::operator delete(p);
        }

        size_type max_size() const throw() { return (std::numeric_limits<size_type>::max() / sizeof(value_type));}

        void construct (pointer p, const_reference val)                         // constructs an object in allocated storage.
        {
            new(p) value_type(val);
        }
        
        void destroy (pointer p)
        {
            p->~value_type();
        }
    };  // class allocator
} // namespace ft

// https://www.cplusplus.com/reference/memory/allocator/
// https://www.codeproject.com/Articles/4795/C-Standard-Allocator-An-Introduction-and-Implement

#endif
