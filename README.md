# ft_containers

Ré-implémentation des nombreux containers / itérateurs du C++. (map, stack, vector, list, queue ...)

Lire le [sujet][1]

## Création et lancement du projet
1. Télécharger / Cloner le dépot
```
git clone https://gitlab.com/flmarsil42/ft_containers && cd ft_containers
```

2. Lancer le projet avec son testeur
```
Make
./testor.out
```

## Sources

- [Best website for learning about C++ (including C++98)][17]
- [Difference of keywords 'typename' and 'class' in templates][2]
- [Lvalus and rvalues][3]
- [Dependant names in C++][4]
- [What's explicit keyword][5]
- [Explicit call to a constructor][15]
- [Implementing iterators][6]
- [Writing your own STL style container][7]
- [Writing operator++ for iterator][8]
- [Writing operator== for iterator][9]
- [When use typename keyword][10]
- [Cours sur les templates][11]
- [Understanding SFINAE (used in enable_if)][12]
- [How to use the allocator][13]
- [Difference between explicit and implicit copy constructor][14]

[1]: https://gitlab.com/flmarsil42/ft_containers/-/blob/master/en.subject.pdf
[2]: https://stackoverflow.com/questions/2023977/difference-of-keywords-typename-and-class-in-templates
[3]: https://www.tutorialspoint.com/What-are-Lvalues-and-Rvalues-in-Cplusplus#:~:text=An%20lvalue%20(locator%20value)%20represents,some%20identifiable%20location%20in%20memory.
[4]: https://stackoverflow.com/questions/1527849/how-do-you-understand-dependent-names-in-c#:~:text=A%20dependent%20name%20is%20essentially,depends%20on%20a%20template%20argument.&text=Names%20that%20depend%20on%20a,at%20the%20point%20of%20definition.
[5]: https://stackoverflow.com/questions/121162/what-does-the-explicit-keyword-mean
[6]: https://stackoverflow.com/questions/8054273/how-to-implement-an-stl-style-iterator-and-avoid-common-pitfalls
[7]: https://stackoverflow.com/questions/7758580/writing-your-own-stl-container/7759622#7759622
[8]: https://stackoverflow.com/questions/4329677/increment-operator-iterator-implementation
[9]: https://stackoverflow.com/questions/12806657/writing-an-operator-function-for-an-iterator-in-c
[10]: https://stackoverflow.com/questions/7923369/when-is-the-typename-keyword-necessary
[11]: https://cpp.developpez.com/cours/cpp/?page=page_14
[12]: https://en.wikibooks.org/wiki/More_C%2B%2B_Idioms/SFINAE
[13]: https://stackoverflow.com/questions/59539057/c-does-it-lead-to-double-free-when-destroy-element-in-allocatorstring
[14]: https://stackoverflow.com/questions/1051379/is-there-a-difference-between-copy-initialization-and-direct-initialization
[15]: https://stackoverflow.com/questions/12036037/explicit-call-to-a-constructor
[17]: http://www.cplusplus.com/
