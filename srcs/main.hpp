#ifndef MAIN_HPP
#define MAIN_HPP

# define RED "\033[1;31m"
# define GREEN "\033[1;32m"
# define BLUE "\033[1;34m"
# define CYAN "\033[1;36m"
# define YELLOW "\033[1;33m"
# define RESET "\033[0m"

// libraries
#include <sstream>
#include <fstream>
#include <typeinfo>
#include <cstdio>
#include <memory>
#include <algorithm>
#include <list>
#include <limits>
#include <cstddef>
#include <cmath>
#include <functional>

#include <vector>
#include <list> 
#include <map>
#include <stack>
#include <queue>
#include <iterator>

/*** FT_CONTAINERS ***/

// libraries
#include <iostream>
#include <exception>

// templates
#include "../includes/Templates/Allocator.hpp"
#include "../includes/Templates/TypeConst.hpp"

// iterators
#include "../includes/Iterators/Node.hpp"
#include "../includes/Iterators/RandomAccess.hpp"
#include "../includes/Iterators/Bidirectional.hpp"
#include "../includes/Iterators/Map_iterator.hpp"


// containters
#include "../includes/Containers/Vector.hpp"
#include "../includes/Containers/List.hpp"
#include "../includes/Containers/Map.hpp"
#include "../includes/Containers/Queue.hpp"
#include "../includes/Containers/Stack.hpp"

/*** TESTOR ***/

// testor template
#include "testor/utils.hpp"
#include "testor/vector_test_template.hpp"
#include "testor/list_test_template.hpp"
#include "testor/map_test_template.hpp"
#include "testor/stack_test_template.hpp"
#include "testor/queue_test_template.hpp"


#endif
