#ifndef UTILS_HPP
#define UTILS_HPP

#include "../main.hpp"

template<typename T>
bool Compare_my(const T &a, const T &b) { return (a > b); }

template<typename T>
bool Predict(const T &a) { return (a == a);}

// type generator
int int_generator ()
{
    return (rand() % 1453934);
}

float float_generator()
{
    return ((rand() % 1453934) / 100.0f);
}

double double_generator()
{
    return ((rand() % 1453934) / 100.0);
}

char char_generator()
{
    int index = rand() % 100;
    return (static_cast<char>((index < 33) ? index + 30 : index));
}

std::string string_generator()
{
    std::string str[10] = {"Hello World !", "Oh my god ...", "42 born to code", "I live in Paris", "ID 2030", "I don't know", "Whoami", "nmap -A -p- 192.168.1.0/24", "nikto -h https://google.com", "curl ifconfig.me"};
    int index = rand() % 10;
    return (str[index]);
}

// check iterators
template<typename T, typename K>
bool check_iterators(T it, T ite, K ft_it, K ft_ite)
{
    for (; it != ite && ft_it != ft_ite ; ++it, ++ft_it)
        if (*it != *ft_it)
            return (false);
    if (it != ite || ft_it != ft_ite)
        return (false);
    return (true);
}

// print
void program_header()
{
    std::cout << GREEN << "\n";
    std::cout << " _____ ___ ___ _____ ___  ___    ___ ___  _  _ _____ _   ___ _  _ ___ ___ ___" << std::endl;
    std::cout << "|_   _| __/ __|_   _/ _ \\| _ \\  / __/ _ \\| \\| |_   _/_\\ |_ _| \\| | __| _ / __|" << std::endl;
    std::cout << "  | | | _|\\__ \\ | || (_) |   / | (_| (_) | .` | | |/ _ \\ | || .` | _||   \\__ \\" << std::endl;
    std::cout << "  |_| |___|___/ |_| \\___/|_|_\\  \\___\\___/|_|\\_| |_/_/ \\_|___|_|\\_|___|_|_|___/" << std::endl;        
    std::cout << RESET;

    std::cout << BLUE << "\n";
    std::cout << "This program will test your different containers and compare them with the official STL library." << std::endl;
    std::cout << "Progress status will be displayed on the screen and the result will be recorded in a " << RED << "LOG file (srcs/testor/logs/)" << BLUE << "!\n\n" << RESET;

    std::cout << GREEN << "1. " << RESET << "Vector\n";
    std::cout << GREEN << "2. " << RESET << "List\n";
    std::cout << GREEN << "3. " << RESET << "Map\n";
    std::cout << GREEN << "4. " << RESET << "Stack\n";
    std::cout << GREEN << "5. " << RESET << "Queue\n";
    std::cout << GREEN << "0. " << RESET << "Exit\n";
    std::cout << BLUE << "Choose a number: " << RESET;
}

#endif
