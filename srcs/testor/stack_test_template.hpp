#ifndef STACK_TEST_TEMPLATE_HPP
#define STACK_TEST_TEMPLATE_HPP

#include "../main.hpp"

template <typename T>
bool stack_check_constructor(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 stack_check_constructor: \n" << RESET;
    os << "\n~ stack (const container_type& ctnr) :\n\n";
    os << "\n~ Empty constructor:\n\n";
    std::stack<T> stack1;
    ft::stack<T> ft_stack1;

    os << "\n[stack] :\t\t" << stack1.size() << std::endl;
    os << "[ft_stack] :\t\t" << ft_stack1.size() << std::endl;

    if (stack1.size() != ft_stack1.size())
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nCreaty constructor with vector x3\n";

    std::vector<T> vec(3, data_a);
    ft::vector<T> ft_vec(3, data_a);

    std::stack<T, std::vector<T> > stack2(vec);
    ft::stack<T, ft::vector<T> > ft_stack2(ft_vec);

    os << "\n[stack.size] :\t\t" << stack2.size() << std::endl;
    os << "[ft_stack.size] :\t" << ft_stack2.size() << std::endl;

    if (stack2.size() != ft_stack2.size())
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;
    *p_log << os.str();
    return (true);
}

template <typename T>
bool stack_check_empty(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 stack_check_empty: \n" << RESET;
    os << "\nCreaty stack empty \n";

    std::stack<T> stack1;
    ft::stack<T> ft_stack1;

    os << "\n[stack.empty] :\t\t" << stack1.empty() << std::endl;
    os << "[ft_stack.empty] :\t" << ft_stack1.empty() << std::endl;

    if (stack1.empty() != ft_stack1.empty())
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nCreaty stack with vector -> data_a \n";

    std::vector<T> vec(3, data_a);
    ft::vector<T> ft_vec(3, data_a);

    std::stack<T, std::vector<T> > stack2(vec);
    ft::stack<T, ft::vector<T> > ft_stack2(ft_vec);

    os << "\n[stack.empty] :\t\t" << stack2.empty() << std::endl;
    os << "[ft_stack.empty] :\t" << ft_stack2.empty() << std::endl;

    if (stack2.empty() != ft_stack2.empty())
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nCreaty stack with vector -> data_b \n";

    vec.assign(12, data_b);
    ft_vec.assign(12, data_b);

    std::stack<T, std::vector<T> > stack3(vec);
    ft::stack<T, ft::vector<T> > ft_stack3(ft_vec);

    os << "\n[stack.empty] :\t\t" << stack3.empty() << std::endl;
    os << "[ft_stack.empty] :\t" << ft_stack3.empty() << std::endl;

    if (stack3.empty() != ft_stack3.empty())
    {
        *p_log << os.str();
        return (false);
    }
    
    *p_log << os.str();
    return (true);
}

template<typename T>
bool stack_check_size(std::ofstream *p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 stack_check_size: \n" << RESET;
    os << "\nCreaty stack empty\n";

    std::stack<T> stack1;
    ft::stack<T> ft_stack1;

    os << "\n[stack.size] :\t\t" << stack1.size() << std::endl;
    os << "[ft_stack.size] :\t" << ft_stack1.size() << std::endl;

    if (stack1.size() != ft_stack1.size())
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nCreaty stack with vector -> data_a\n";

    std::vector<T> vec(3, data_a);
    ft::vector<T> ft_vec(3, data_a);

    std::stack<T, std::vector<T> > stack2(vec);
    ft::stack<T, ft::vector<T> > ft_stack2(ft_vec);

    os << "\n[stack.size] :\t\t" << stack2.size() << std::endl;
    os << "[ft_stack.size] :\t" << ft_stack2.size() << std::endl;

    if (stack2.size() != ft_stack2.size())
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nCreaty stack with vector -> data_b \n";

    vec.assign(30, data_b);
    ft_vec.assign(30, data_b);

    std::stack<T, std::vector<T> > stack3(vec);
    ft::stack<T, ft::vector<T> > ft_stack3(ft_vec);

    os << "\n[stack.size] :\t\t" << stack3.size() << std::endl;
    os << "[ft_stack.size] :\t" << ft_stack3.size() << std::endl;

    if (stack3.size() != ft_stack3.size())
    {
        *p_log << os.str();
        return (false);
    }
    
    *p_log << os.str();
    return (true);
}

template<typename T>
bool stack_check_top(std::ofstream *p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 stack_check_top: \n" << RESET;
    os << "\nCreaty stack with vector -> data_a\n";

    std::vector<T> vec(3, data_a);
    ft::vector<T> ft_vec(3, data_a);

    std::stack<T, std::vector<T> > stack2(vec);
    ft::stack<T, ft::vector<T> > ft_stack2(ft_vec);

    os << "\n[stack.size] :\t\t" << stack2.top() << std::endl;
    os << "[ft_stack.size] :\t" << ft_stack2.top() << std::endl;

    if (stack2.top() != ft_stack2.top())
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nCreaty stack with vector -> data_b \n";

    vec.assign(7, data_b);
    ft_vec.assign(7, data_b);

    std::stack<T, std::vector<T> > stack3(vec);
    ft::stack<T, ft::vector<T> > ft_stack3(ft_vec);

    os << "\n[stack.size] :\t\t" << stack3.top() << std::endl;
    os << "[ft_stack.size] :\t" << ft_stack3.top() << std::endl;

    if (stack3.top() != ft_stack3.top())
    {
        *p_log << os.str();
        return (false);
    }
    
    *p_log << os.str();
    return (true);
}

template<typename T>
bool stack_check_pop(std::ofstream *p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 stack_check_pop: \n" << RESET;
    os << "\nCreaty stack with vector -> data_a\n";

    std::vector<T> vec(3, data_a);
    ft::vector<T> ft_vec(3, data_a);

    std::stack<T, std::vector<T> > stack2(vec);
    ft::stack<T, ft::vector<T> > ft_stack2(ft_vec);

    os << "\n[stack.size] :\t\t" << stack2.size() << std::endl;
    os << "[ft_stack.size] :\t" << ft_stack2.size() << std::endl;
    
    if (stack2.size() != ft_stack2.size())
    {
        *p_log << os.str();
        return (false);
    }

    stack2.pop();
    ft_stack2.pop();
    os << "\n[stack.size] :\t\t" << stack2.size() << std::endl;
    os << "[ft_stack.size] :\t" << ft_stack2.size() << std::endl;

    if (stack2.size() != ft_stack2.size())
    {
        *p_log << os.str();
        return (false);
    }
    
    stack2.pop();
    ft_stack2.pop();
    os << "\n[stack.size] :\t\t" << stack2.size() << std::endl;
    os << "[ft_stack.size] :\t" << ft_stack2.size() << std::endl;

    if (stack2.size() != ft_stack2.size())
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nCreaty stack with vector -> data_b \n";

    vec.assign(30, data_b);
    ft_vec.assign(30, data_b);

    std::stack<T, std::vector<T> > stack3(vec);
    ft::stack<T, ft::vector<T> > ft_stack3(ft_vec);

    os << "\n[stack.size] :\t\t" << stack3.size() << std::endl;
    os << "[ft_stack.size] :\t" << ft_stack3.size() << std::endl;
    
    if (stack3.size() != ft_stack3.size())
    {
        *p_log << os.str();
        return (false);
    }

    stack3.pop();
    ft_stack3.pop();
    os << "\n[stack.size] :\t\t" << stack3.size() << std::endl;
    os << "[ft_stack.size] :\t" << ft_stack3.size() << std::endl;

    if (stack3.size() != ft_stack3.size())
    {
        *p_log << os.str();
        return (false);
    }
    
    stack3.pop();
    ft_stack3.pop();
    os << "\n[stack.size] :\t\t" << stack3.size() << std::endl;
    os << "[ft_stack.size] :\t" << ft_stack3.size() << std::endl;

    if (stack3.size() != ft_stack3.size())
    {
        *p_log << os.str();
        return (false);
    }
    
    *p_log << os.str();
    return (true);
}

template<typename T>
bool stack_check_push(std::ofstream *p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 stack_check_push: \n" << RESET;
    os << "\nCreaty stack with vector -> data_a data_b\n";

    std::stack<T> stack;
    ft::stack<T> ft_stack;
    
    for (size_t i = 0; i < 5; i++)
    {
        stack.push(data_a);
        ft_stack.push(data_a);
    }

    for (size_t i = 0; i < 3; i++)
    {
        stack.push(data_b);
        ft_stack.push(data_b);
    }

    size_t i = 0;

    while (!stack.empty() && !ft_stack.empty())
    {
        if (stack.top() != ft_stack.top())
        {
            *p_log << os.str();
            return (false);
        }
        os << "\nNumber cicle: " << ++i << "\n[stack] :\t\t" << stack.top() << std::endl;
        os << "[ft_stack] :\t\t" << ft_stack.top() << std::endl;
        stack.pop();
        ft_stack.pop();
    }
    if (!ft_stack.empty() || !stack.empty())
    {
        *p_log << os.str();
        return (false);
    }     
    
    *p_log << os.str();
    return (true);
}

template<typename T>
bool stack_check_relational(std::ofstream *p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 stack_check_relational: \n" << RESET;
    os << "\nCreaty stack empty\n";

    std::stack<T> stack1;
    std::stack<T> stack2;
    ft::stack<T> ft_stack1;
    ft::stack<T> ft_stack2;

    os << "\n[stack]: After operator== " << (stack1 == stack2) << std::endl;
    os << "\n[ft_stack]: After operator== " << (ft_stack1 == ft_stack2) << std::endl;
    if ((stack1 == stack2) != (ft_stack1 == ft_stack2))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[stack]: After operator!= " << (stack1 != stack2) << std::endl;
    os << "\n[ft_stack]: After operator!= " << (ft_stack1 != ft_stack2) << std::endl;
    if ((stack1 != stack2) != (ft_stack1 != ft_stack2))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[stack]: After operator< " << (stack1 < stack2) << std::endl;
    os << "\n[ft_stack]: After operator< " << (ft_stack1 < ft_stack2) << std::endl;
    if ((stack1 < stack2) != (ft_stack1 < ft_stack2))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[stack]: After operator<= " << (stack1 <= stack2) << std::endl;
    os << "\n[ft_stack]: After operator<= " << (ft_stack1 <= ft_stack2) << std::endl;
    if ((stack1 <= stack2) != (ft_stack1 <= ft_stack2))
    {
        *p_log << os.str();
        return (false);
    }
    
    os << "\n[stack]: After operator> " << (stack1 > stack2) << std::endl;
    os << "\n[ft_stack]: After operator> " << (ft_stack1 > ft_stack2) << std::endl;
    if ((stack1 > stack2) != (ft_stack1 > ft_stack2))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[stack]: After operator>= " << (stack1 >= stack2) << std::endl;
    os << "\n[ft_stack]: After operator>= " << (ft_stack1 >= ft_stack2) << std::endl;
    if ((stack1 >= stack2) != (ft_stack1 >= ft_stack2))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nCreaty stack with vector -> data_a data_b\n";

    for (size_t i = 0; i < 5; i++)
    {
        stack1.push(data_a);
        ft_stack1.push(data_a);
    }

    for (size_t i = 0; i < 5; i++)
    {
        stack2.push(data_b);
        ft_stack2.push(data_b);
    }

    os << "\n[stack]: After operator== " << (stack1 == stack2) << std::endl;
    os << "\n[ft_stack]: After operator== " << (ft_stack1 == ft_stack2) << std::endl;
    if ((stack1 == stack2) != (ft_stack1 == ft_stack2))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[stack]: After operator!= " << (stack1 != stack2) << std::endl;
    os << "\n[ft_stack]: After operator!= " << (ft_stack1 != ft_stack2) << std::endl;
    if ((stack1 != stack2) != (ft_stack1 != ft_stack2))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[stack]: After operator< " << (stack1 < stack2) << std::endl;
    os << "\n[ft_stack]: After operator< " << (ft_stack1 < ft_stack2) << std::endl;
    if ((stack1 < stack2) != (ft_stack1 < ft_stack2))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[stack]: After operator<= " << (stack1 <= stack2) << std::endl;
    os << "\n[ft_stack]: After operator<= " << (ft_stack1 <= ft_stack2) << std::endl;
    if ((stack1 <= stack2) != (ft_stack1 <= ft_stack2))
    {
        *p_log << os.str();
        return (false);
    }
    
    os << "\n[stack]: After operator> " << (stack1 > stack2) << std::endl;
    os << "\n[ft_stack]: After operator> " << (ft_stack1 > ft_stack2) << std::endl;
    if ((stack1 > stack2) != (ft_stack1 > ft_stack2))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n[stack]: After operator>= " << (stack1 >= stack2) << std::endl;
    os << "\n[ft_stack]: After operator>= " << (ft_stack1 >= ft_stack2) << std::endl;
    if ((stack1 >= stack2) != (ft_stack1 >= ft_stack2))
    {
        *p_log << os.str();
        return (false);
    }

    *p_log << os.str();
    return (true);
}


template <typename T>
void stack_run(std::ofstream* out, T (*f)())
{
    std::cout << BLUE << "\n[check constructor]: \t" << ((stack_check_constructor<T>(out, f(), f())) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check empty]: \t\t" << ((stack_check_empty<T>(out, f(), f())) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check size]: \t\t" << ((stack_check_size<T>(out, f(), f())) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check top]: \t\t" << ((stack_check_top<T>(out, f(), f())) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check pop]: \t\t" << ((stack_check_pop<T>(out, f(), f())) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check push]: \t\t" << ((stack_check_push<T>(out, f(), f())) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check relational]: \t" << ((stack_check_relational<T>(out, f(), f())) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << RESET << std::endl;
}

template <typename T>
void stack_launcher(std::ofstream* p_log)
{
    if (typeid(T) == typeid(int))
        stack_run<int>(p_log, int_generator);
    else if (typeid(T) == typeid(float))
        stack_run<float>(p_log, float_generator);
    else if (typeid(T) == typeid(double))
        stack_run<double>(p_log, double_generator);
    else if (typeid(T) == typeid(char))
        stack_run<char>(p_log, char_generator);
    else if (typeid(T) == typeid(std::string))
        stack_run<std::string>(p_log, string_generator);
}

#endif
