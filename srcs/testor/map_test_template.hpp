#ifndef MAP_TEST_TEMPLATE_HPP
#define MAP_TEST_TEMPLATE_HPP

#include "../main.hpp"

void print_map(std::stringstream &os, std::map<int, std::string> &map, ft::map<int, std::string> &ft_map)
{
    std::map<int, std::string>::iterator it = map.begin();
    std::map<int, std::string>::iterator ite = map.end();
    ft::map<int, std::string>::iterator ft_it = ft_map.begin();
    ft::map<int, std::string>::iterator ft_ite = ft_map.end();

    os << "\nmap : \t\t\n";
    for (;it != ite; ++it)
        os << it->first << " = " << it->second << std::endl;

    os << "\nft_map: \t\n";
    for (; ft_it != ft_ite ; ++ft_it)
        os << ft_it->first << " = " << ft_it->second << std::endl;

    std::map<int, std::string>::reverse_iterator r_it = map.rbegin();
    std::map<int, std::string>::reverse_iterator r_ite = map.rend();
    ft::map<int, std::string>::reverse_iterator ft_r_it = ft_map.rbegin();
    ft::map<int, std::string>::reverse_iterator ft_r_ite = ft_map.rend();

    os << "\nmap (reverse): \t\n";
    for (; r_it != r_ite ; ++r_it)
        os << r_it->first << " = " << r_it->second << std::endl;

    os << "\nft_map (reverse): \t\n";
    for(; ft_r_it != ft_r_ite ; ++ft_r_it)
        os << ft_r_it->first << " = " << ft_r_it->second << std::endl;
}

void print_size_map(std::stringstream &os, std::map<int, std::string> &map, ft::map<int, std::string> &ft_map)
{
    os << "\nSize map: " << map.size() << std::endl;
    os << "\nSize ft_map: " << ft_map.size() << std::endl;
}

bool check_map(std::map<int, std::string> &map, ft::map<int, std::string> &ft_map)
{
    std::map<int, std::string>::iterator it = map.begin();
    std::map<int, std::string>::iterator ite = map.end();
    ft::map<int, std::string>::iterator ft_it = ft_map.begin();
    ft::map<int, std::string>::iterator ft_ite = ft_map.end();

    for (; it != ite && ft_it != ft_ite ; ++it, ++ft_it)
        if (it->first != ft_it->first || it->second != ft_it->second)
            return (false);

    if (ft_it != ft_ite)
        return (false);

    std::map<int, std::string>::reverse_iterator r_it = map.rbegin();
    std::map<int, std::string>::reverse_iterator r_ite = map.rend();
    ft::map<int, std::string>::reverse_iterator ft_r_it = ft_map.rbegin();
    ft::map<int, std::string>::reverse_iterator ft_r_ite = ft_map.rend();
    for (; r_it != r_ite && ft_r_it != ft_r_ite ; ++r_it, ++ft_r_it)
        if (r_it->first != ft_r_it->first || r_it->second != ft_r_it->second)
            return (false);

    if (map.size() != ft_map.size())
        return (false);

    return (true);
}

bool map_check_constructor(std::ofstream *out)
{
    std::stringstream os;
    os << BLUE << "\n🤖 map_check_constructor : \n" << RESET;
    os << "\n~ Empty constructor:\n\n";

    std::map<int, std::string> map1;
    ft::map<int, std::string> ft_map1;

    print_map(os, map1, ft_map1);
    print_size_map(os, map1, ft_map1);
    if (!check_map(map1, ft_map1))
    {
        *out << os.str();
        return (false);
    }

    std::map<int,std::string> first;

    first[1]="10";
    first[2]="30";
    first[3]="50";
    first[4]="70";

    ft::map<int,std::string> second;

    second[1]="10";
    second[2]="30";
    second[3]="50";
    second[4]="70";

    os << "\n~ Constructor (iterator first, iterator last) :\n\n";

    std::map<int, std::string> map2(first.begin(), first.end());
    ft::map<int, std::string> ft_map2(second.begin(), second.end());
    print_map(os, map2, ft_map2);
    print_size_map(os, map2, ft_map2);
    if (map2.size() != ft_map2.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(map2, ft_map2))
    {
        *out << os.str();
        return (false);
    }

    std::map<int, std::string>::const_iterator it = first.begin();
    std::map<int, std::string>::const_iterator ite = first.end();
    ft::map<int, std::string>::const_iterator ft_it = second.begin();
    ft::map<int, std::string>::const_iterator ft_ite = second.end();

    os << "\n~ Constructor (const_iterator first, const_iterator last) :\n\n";

    std::map<int, std::string> map3(it, ite);
    ft::map<int, std::string> ft_map3(ft_it, ft_ite);
    print_map(os, map3, ft_map3);
    print_size_map(os, map3, ft_map3);
    if (!check_map(map3, ft_map3))
    {
        *out << os.str();
        return (false);
    }

    os << "\n~ Constructor (const map& x) :\n\n";

    std::map<int, std::string> map4(first);
    ft::map<int, std::string> ft_map4(second);
    print_map(os, map4, ft_map4);
    print_size_map(os, map4, ft_map4);
    if (!check_map(map4, ft_map4))
    {
        *out << os.str();
        return (false);
    }

    *out << os.str();
    return (true);
}

bool map_check_operator_equal(std::ofstream *out)
{
    std::stringstream os;
    os << BLUE << "\n🤖 map_check_operator : \n" << RESET;
    os << "\nCreaty map(map1 ft_map1)\n";
    std::map<int,std::string> first;

    first[1]="10";
    first[2]="30";
    first[3]="50";
    first[4]="70";

    ft::map<int,std::string> second;

    second[1]="10";
    second[2]="30";
    second[3]="50";
    second[4]="70";

    os << "\nTest#1 operator=\n";
    std::map<int, std::string> map1;
    ft::map<int, std::string> ft_map1;

    map1 = first;
    ft_map1 = second;
    print_map(os, map1, ft_map1);
    print_size_map(os, map1, ft_map1);
    if (map1.size() != ft_map1.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(map1, ft_map1))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#2 operator=\n";
    std::map<int, std::string> map2;
    ft::map<int, std::string> ft_map2;

    map2 = map1;
    ft_map2 = ft_map1;
    print_map(os, map2, ft_map2);
    print_size_map(os, map2, ft_map2);
    if (map2.size() != ft_map2.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(map2, ft_map2))
    {
        *out << os.str();
        return (false);
    }

    *out << os.str();
    return (true);
}

bool map_check_iterator(std::ofstream *out)
{
    std::stringstream os;
    os << BLUE << "\n🤖 map_check_iterator : \n" << RESET;
    os << "\nCreaty map(first second)\n";
    std::map<int,std::string> first;

    first[1]="10";
    first[2]="30";
    first[3]="50";
    first[4]="70";

    ft::map<int,std::string> second;

    second[1]="10";
    second[2]="30";
    second[3]="50";
    second[4]="70";

    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#1 iterator\n";
    std::map<int, std::string>::iterator it_1 = first.begin();
    std::map<int, std::string>::iterator ite_1 = first.end();
    ft::map<int, std::string>::iterator ft_it_1 = second.begin();
    ft::map<int, std::string>::iterator ft_ite_1 = second.end();
    
    os << "map : \t\n";
    while (it_1 != ite_1)
    {
        os << it_1->first << " = " << it_1->second << std::endl;
        ++it_1;
    }

    os << "ft_map: \t\n";
    while (ft_it_1 != ft_ite_1)
    {
        os << ft_it_1->first << " = " << ft_it_1->second << std::endl;
        ++ft_it_1;
    }

    os << "\nTest#2 const_iterator\n";
    std::map<int, std::string>::const_iterator it_2 = first.begin();
    std::map<int, std::string>::const_iterator ite_2 = first.end();
    ft::map<int, std::string>::const_iterator ft_it_2 = second.begin();
    ft::map<int, std::string>::const_iterator ft_ite_2 = second.end();
    os << "map : \t\n";
    while (it_2 != ite_2)
    {
        os << it_2->first << " = " << it_2->second << std::endl;
        ++it_2;
    }

    os << "ft_map: \t\n";
    while (ft_it_2 != ft_ite_2)
    {
        os << ft_it_2->first << " = " << ft_it_2->second << std::endl;
        ++ft_it_2;
    }

    os << "\nTest#3 reverse_iterator\n";
    std::map<int, std::string>::reverse_iterator it_3 = first.rbegin();
    std::map<int, std::string>::reverse_iterator ite_3 = first.rend();
    ft::map<int, std::string>::reverse_iterator ft_it_3 = second.rbegin();
    ft::map<int, std::string>::reverse_iterator ft_ite_3 = second.rend();
    os << "map : \t\n";
    while (it_3 != ite_3)
    {
        os << it_3->first << " = " << it_3->second << std::endl;
        ++it_3;
    }

    os << "ft_map: \t\n";
    while (ft_it_3 != ft_ite_3)
    {
        os << ft_it_3->first << " = " << ft_it_3->second << std::endl;
        ++ft_it_3;
    }

    os << "\nTest#4 const_reverse_iterator\n";
    std::map<int, std::string>::const_reverse_iterator it_4 = first.rbegin();
    std::map<int, std::string>::const_reverse_iterator ite_4 = first.rend();
    ft::map<int, std::string>::const_reverse_iterator ft_it_4 = second.rbegin();
    ft::map<int, std::string>::const_reverse_iterator ft_ite_4 = second.rend();
    os << "map : \t\n";
    while (it_4 != ite_4)
    {
        os << it_4->first << " = " << it_4->second << std::endl;
        ++it_4;
    }

    os << "ft_map: \t\n";
    while (ft_it_4 != ft_ite_4)
    {
        os << ft_it_4->first << " = " << ft_it_4->second << std::endl;
        ++ft_it_4;
    }

    *out << os.str();
    return (true);
}

bool map_check_empty(std::ofstream *out)
{
    std::stringstream os;
    os << BLUE << "\n🤖 map_check_empty : \n" << RESET;
    os << "\nCreaty map(first second)\n";
    std::map<int,std::string> first;
    ft::map<int,std::string> second;

    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#1 empty map\n";
    os << "Empty map: " << first.empty() << std::endl;
    os << "Empty ft_map: " << second.empty() << std::endl;

    if ((first.empty()) != (second.empty()))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#2 fill map\n";
    first[1]="10";
    first[2]="30";
    first[3]="50";
    first[4]="70";

    second[1]="10";
    second[2]="30";
    second[3]="50";
    second[4]="70";

    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "Empty map: " << first.empty() << std::endl;
    os << "Empty ft_map: " << second.empty() << std::endl;

    if ((first.empty()) != (second.empty()))
    {
        *out << os.str();
        return (false);
    }

    *out << os.str();
    return (true);
}

bool map_check_size(std::ofstream *out)
{
    std::stringstream os;
    os << BLUE << "\n🤖 map_check_size: \n" << RESET;
    os << "\nCreaty map(first second)\n";
    std::map<int,std::string> first;
    ft::map<int,std::string> second;

    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#1 empty map\n";
    os << "size map: " << first.size() << std::endl;
    os << "size ft_map: " << second.size() << std::endl;

    if ((first.size()) != (second.size()))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#2 fill map\n";
    first[1]="10";
    first[2]="30";
    first[3]="50";
    first[4]="70";

    second[1]="10";
    second[2]="30";
    second[3]="50";
    second[4]="70";

    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "size map: " << first.size() << std::endl;
    os << "size ft_map: " << second.size() << std::endl;

    if ((first.size()) != (second.size()))
    {
        *out << os.str();
        return (false);
    }

    *out << os.str();
    return (true);
}

bool map_check_max_size(std::ofstream *out)
{
    std::stringstream os;
    os << BLUE << "\n🤖 map_check_max_size : \n" << RESET;
    os << "\nCreaty map(first second)\n";
    std::map<int,std::string> first;
    ft::map<int,std::string> second;

    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#1 empty map\n";
    os << "max_size map: " << first.max_size() << std::endl;
    os << "max_size ft_map: " << second.max_size() << std::endl;

    if ((first.max_size()) != (second.max_size()))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#2 fill map\n";
    first[1]="10";
    first[2]="30";
    first[3]="50";
    first[4]="70";

    second[1]="10";
    second[2]="30";
    second[3]="50";
    second[4]="70";

    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "max_size map: " << first.max_size() << std::endl;
    os << "max_size ft_map: " << second.max_size() << std::endl;

    if ((first.max_size()) != (second.max_size()))
    {
        *out << os.str();
        return (false);
    }

    *out << os.str();
    return (true);
}

bool map_check_operator_crochet(std::ofstream *out)
{
    std::stringstream os;
    os << BLUE << "\n🤖 map_check_crochet: \n" << RESET;
    os << "\nCreaty map(first second)\n";
    std::map<int,std::string> first;
    ft::map<int,std::string> second;

    first[1]="10";
    first[2]="30";
    first[3]="50";
    first[4]="70";

    second[1]="10";
    second[2]="30";
    second[3]="50";
    second[4]="70";

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#1 operator[1]\n";
    os << "[1] map: " << first[1] << std::endl;
    os << "[1] ft_map: " << second[1] << std::endl;

    if ((first[1]) != (second[1]))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#2 operator[2]\n";
    os << "[2] map: " << first[2] << std::endl;
    os << "[2] ft_map: " << second[2] << std::endl;

    if ((first[2]) != (second[2]))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#3 operator[3]\n";
    os << "[3] map: " << first[3] << std::endl;
    os << "[3] ft_map: " << second[3] << std::endl;

    if ((first[3]) != (second[3]))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#4 operator[4]\n";
    os << "[4] map: " << first[4] << std::endl;
    os << "[4] ft_map: " << second[4] << std::endl;

    if ((first[4]) != (second[4]))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#6 operator[6]\n";
    os << "[6] map: " << first[6] << std::endl;
    os << "[6] ft_map: " << second[6] << std::endl;

    if ((first[6]) != (second[6]))
    {
        *out << os.str();
        return (false);
    }

    *out << os.str();
    return (true);
}

bool map_check_insert(std::ofstream *out)
{
    std::stringstream os;
    os << BLUE << "\n🤖 map_check_insert : \n" << RESET;
    os << "\nCreaty map(first second)\n";
    std::map<int,std::string> first;
    ft::map<int,std::string> second;

    first[1]="10";
    first[2]="30";
    first[3]="50";
    first[4]="70";

    second[1]="10";
    second[2]="30";
    second[3]="50";
    second[4]="70";

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#1 (const value_type& val);\n";
    first.insert(std::pair<int, std::string>(228, "1448228"));
    second.insert(std::pair<int, std::string>(228, "1448228"));

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#2 (const value_type& val);\n";
    first.insert(std::pair<int, std::string>(22, "PiuPiu"));
    second.insert(std::pair<int, std::string>(22, "PiuPiu"));

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#3 (iterator position, const value_type& val)\n";
    first.insert(first.begin(), std::pair<int, std::string>(12, "Oh dawn dude!"));
    second.insert(second.begin(), std::pair<int, std::string>(12, "Oh dawn dude!"));

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#4 (InputIterator first, InputIterator last);\n";
    std::map<int, std::string> map1;
    ft::map<int, std::string> ft_map1;
    map1.insert(first.begin(), first.end());
    ft_map1.insert(second.begin(), second.end());

    print_map(os, map1, ft_map1);
    print_size_map(os, map1, ft_map1);
    if (map1.size() != ft_map1.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(map1, ft_map1))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#5 (InputIterator first, InputIterator last);\n";
    std::map<int, std::string> map2;
    ft::map<int, std::string> ft_map2;
    map2.insert(map1.begin(), map1.end());
    ft_map2.insert(ft_map1.begin(), ft_map1.end());

    print_map(os, map2, ft_map2);
    print_size_map(os, map2, ft_map2);
    if (map2.size() != ft_map2.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(map2, ft_map2))
    {
        *out << os.str();
        return (false);
    }

    *out << os.str();
    return (true);
}

bool map_check_erase(std::ofstream *out)
{
    std::stringstream os;
    os << BLUE << "\n🤖 map_check_erase : \n" << RESET;
    os << "\nCreaty map(first second)\n";
    std::map<int,std::string> first;
    ft::map<int,std::string> second;

    first[1]="10";
    first[2]="30";
    first[3]="50";
    first[4]="70";
    first[5]="101";
    first[6]="301";
    first[7]="501";
    first[8]="701";

    second[1]="10";
    second[2]="30";
    second[3]="50";
    second[4]="70";
    second[5]="101";
    second[6]="301";
    second[7]="501";
    second[8]="701";

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#1 .begin() (iterator position)\n";
    first.erase(first.begin());
    second.erase(second.begin());

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#2 .begin() (iterator position)\n";
    first.erase(first.begin());
    second.erase(second.begin());

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#3 [7] (const key_type& k);\n";
    first.erase(7);
    second.erase(7);

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#4 [5] (const key_type& k);\n";
    first.erase(5);
    second.erase(5);

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#5 ++.begin() --.end() (iterator first, iterator last)\n";
    first.erase(++(first.begin()), --(first.end()));
    second.erase(++(second.begin()), --(second.end()));

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    *out << os.str();
    return (true);
}

bool map_check_swap(std::ofstream *out)
{
    std::stringstream os;
    os << BLUE << "\n🤖 map_check_swap : \n" << RESET;
    os << "\nCreaty map(first second)\n";
    std::map<int,std::string> first;
    ft::map<int,std::string> second;

    first[1]="10";
    first[2]="30";
    first[3]="50";
    first[4]="70";
    first[5]="101";
    first[6]="301";
    first[7]="501";
    first[8]="701";

    second[1]="10";
    second[2]="30";
    second[3]="50";
    second[4]="70";
    second[5]="101";
    second[6]="301";
    second[7]="501";
    second[8]="701";

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#1 swap map1(first) ft_map1(second)\n";
    std::map<int, std::string> map1;
    ft::map<int, std::string> ft_map1;

    map1.swap(first);
    ft_map1.swap(second);

    os << "\nTest#1 after swap first second\n";
    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#1 after swap map1 ft_map1\n";
    print_map(os, map1, ft_map1);
    print_size_map(os, map1, ft_map1);
    if (map1.size() != ft_map1.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(map1, ft_map1))
    {
        *out << os.str();
        return (false);
    }

    *out << os.str();
    return (true);
}

bool map_check_clear(std::ofstream *out)
{
    std::stringstream os;
    os << "\n|========> !(*_*)! map_check_clear \n";
    os << "\nCreaty map(first second)\n";
    std::map<int,std::string> first;
    ft::map<int,std::string> second;

    first[1]="10";
    first[2]="30";
    first[3]="50";
    first[4]="70";
    first[5]="101";
    first[6]="301";
    first[7]="501";
    first[8]="701";

    second[1]="10";
    second[2]="30";
    second[3]="50";
    second[4]="70";
    second[5]="101";
    second[6]="301";
    second[7]="501";
    second[8]="701";

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#1 clear first second\n";
    first.clear();
    second.clear();

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    *out << os.str();
    return (true);
}

bool map_check_key_comp(std::ofstream *out)
{
    std::stringstream os;
    os << BLUE << "\n🤖 map_check_key_comp : \n" << RESET;
    os << "\nCreaty map(first second)\n";
    std::map<int,std::string> first;
    ft::map<int,std::string> second;
    std::map<int,std::string>::key_compare comp_first = first.key_comp();
    ft::map<int,std::string>::key_compare comp_second = second.key_comp();

    first[1]="10";
    first[2]="30";
    first[3]="50";
    first[4]="70";
    first[5]="101";
    first[6]="301";
    first[7]="501";
    first[8]="701";

    second[1]="10";
    second[2]="30";
    second[3]="50";
    second[4]="70";
    second[5]="101";
    second[6]="301";
    second[7]="501";
    second[8]="701";

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }


    os << "\nTest#1\n";
    std::map<int, std::string> map1;
    ft::map<int, std::string> ft_map1;

    std::map<int, std::string>::iterator it = ++(first.begin());
    std::map<int, std::string>::iterator ite = --(first.end());
    ft::map<int, std::string>::iterator ft_it = ++(second.begin());
    ft::map<int, std::string>::iterator ft_ite = --(second.end());

    while (comp_first(it->first, ite->first))
    {
        map1.insert(std::pair<int, std::string>(it->first, it->second));
        ++it;
    }

    while (comp_second(ft_it->first, ft_ite->first))
    {
        ft_map1.insert(std::pair<int, std::string>(ft_it->first, ft_it->second));
        ++ft_it;
    }

    print_map(os, map1, ft_map1);
    print_size_map(os, map1, ft_map1);
    if (map1.size() != ft_map1.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(map1, ft_map1))
    {
        *out << os.str();
        return (false);
    }

    *out << os.str();
    return (true);
}

bool map_check_value_comp(std::ofstream *out)
{
    std::stringstream os;
    os << BLUE << "\n🤖 map_check_value_comp : \n" << RESET;
    os << "\nCreaty map(first second)\n";
    std::map<int,std::string> first;
    ft::map<int,std::string> second;
    std::map<int,std::string>::value_compare comp_first = first.value_comp();
    ft::map<int,std::string>::value_compare comp_second = second.value_comp();

    first[1]="10";
    first[2]="30";
    first[3]="50";
    first[4]="70";
    first[5]="101";
    first[6]="301";
    first[7]="501";
    first[8]="701";

    second[1]="10";
    second[2]="30";
    second[3]="50";
    second[4]="70";
    second[5]="101";
    second[6]="301";
    second[7]="501";
    second[8]="701";

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }


    os << "\nTest#1\n";
    std::map<int, std::string> map1;
    ft::map<int, std::string> ft_map1;

    std::map<int, std::string>::iterator it = ++(first.begin());
    std::map<int, std::string>::iterator ite = --(first.end());
    ft::map<int, std::string>::iterator ft_it = ++(second.begin());
    ft::map<int, std::string>::iterator ft_ite = --(second.end());

    while (comp_first(*it, *ite))
    {
        map1.insert(std::pair<int, std::string>(it->first, it->second));
        ++it;
    }

    while (comp_second(*ft_it, *ft_ite))
    {
        ft_map1.insert(std::pair<int, std::string>(ft_it->first, ft_it->second));
        ++ft_it;
    }

    print_map(os, map1, ft_map1);
    print_size_map(os, map1, ft_map1);
    if (map1.size() != ft_map1.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(map1, ft_map1))
    {
        *out << os.str();
        return (false);
    }

    *out << os.str();
    return (true);
}

bool map_check_find(std::ofstream *out)
{
    std::stringstream os;
    os << BLUE << "\n🤖 map_check_find : \n" << RESET;
    os << "\nCreaty map(first second)\n";
    std::map<int,std::string> first;
    ft::map<int,std::string> second;

    first[1]="10";
    first[2]="30";
    first[3]="50";
    first[4]="70";
    first[5]="101";
    first[6]="301";
    first[7]="501";
    first[8]="701";

    second[1]="10";
    second[2]="30";
    second[3]="50";
    second[4]="70";
    second[5]="101";
    second[6]="301";
    second[7]="501";
    second[8]="701";

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#1 find(2)\n";
    std::map<int, std::string>::const_iterator first_it = first.find(2);
    ft::map<int, std::string>::const_iterator second_it = second.find(2);

    os << "map: " << first_it->first << " = " << first_it->second << std::endl;
    os << "ft_map: " << second_it->first << " = " << second_it->second << std::endl;

    if (first_it->first != second_it->first || first_it->second != second_it->second)
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#2 find(4)\n";
    first_it = first.find(4);
    second_it = second.find(4);

    os << "map: " << first_it->first << " = " << first_it->second << std::endl;
    os << "ft_map: " << second_it->first << " = " << second_it->second << std::endl;

    if (first_it->first != second_it->first || first_it->second != second_it->second)
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#2 find(5)\n";
    first_it = first.find(5);
    second_it = second.find(5);

    os << "map: " << first_it->first << " = " << first_it->second << std::endl;
    os << "ft_map: " << second_it->first << " = " << second_it->second << std::endl;

    if (first_it->first != second_it->first || first_it->second != second_it->second)
    {
        *out << os.str();
        return (false);
    }

    *out << os.str();
    return (true);
}

bool map_check_count(std::ofstream *out)
{
    std::stringstream os;
    os << BLUE << "\n🤖 map_check_count : \n" << RESET;
    os << "\nCreaty map(first second)\n";
    std::map<int,std::string> first;
    ft::map<int,std::string> second;

    first[1]="10";
    first[2]="30";
    first[3]="50";
    first[4]="70";
    first[5]="101";
    first[6]="301";
    first[7]="501";
    first[8]="701";

    second[1]="10";
    second[2]="30";
    second[3]="50";
    second[4]="70";
    second[5]="101";
    second[6]="301";
    second[7]="501";
    second[8]="701";

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#1 count(1)\n";
    os << "Count map: " << first.count(1) << std::endl;
    os << "Count ft_map: " << second.count(1) << std::endl;

    if (first.count(1) != second.count(1))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#2 count(11)\n";
    os << "Count map: " << first.count(11) << std::endl;
    os << "Count ft_map: " << second.count(11) << std::endl;

    if (first.count(11) != second.count(11))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#3 count(8)\n";
    os << "Count map: " << first.count(8) << std::endl;
    os << "Count ft_map: " << second.count(8) << std::endl;

    if (first.count(8) != second.count(8))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#4 count(228)\n";
    first.insert(std::pair<int, std::string>(228, "1488228"));
    second.insert(std::pair<int, std::string>(228, "1488228"));
    os << "Count map: " << first.count(228) << std::endl;
    os << "Count ft_map: " << second.count(228) << std::endl;

    if (first.count(228) != second.count(228))
    {
        *out << os.str();
        return (false);
    }

    *out << os.str();
    return (true);
}

bool map_check_lower_bound(std::ofstream *out)
{
    std::stringstream os;
    os << BLUE << "\n🤖 map_check_lower_bound : \n" << RESET;
    os << "\nCreaty map(first second)\n";
    std::map<int,std::string> first;
    ft::map<int,std::string> second;
    std::map<int,std::string>::iterator first_it;
    ft::map<int,std::string>::iterator second_it;

    first[1]="10";
    first[2]="30";
    first[3]="50";
    first[4]="70";
    first[5]="101";
    first[6]="301";
    first[7]="501";
    first[8]="701";

    second[1]="10";
    second[2]="30";
    second[3]="50";
    second[4]="70";
    second[5]="101";
    second[6]="301";
    second[7]="501";
    second[8]="701";

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#1 lower_bound(1)\n";
    first_it = first.lower_bound(1);
    second_it = second.lower_bound(1);
    os << "lower_bound map: " << first_it->first << std::endl;
    os << "lower_bound ft_map: " << second_it->first << std::endl;

    if (first_it->first != second_it->first)
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#2 lower_bound(2)\n";
    first_it = first.lower_bound(2);
    second_it = second.lower_bound(2);
    os << "lower_bound map: " << first_it->first << std::endl;
    os << "lower_bound ft_map: " << second_it->first << std::endl;

    if (first_it->first != second_it->first)
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#3 lower_bound(7)\n";
    first_it = first.lower_bound(7);
    second_it = second.lower_bound(7);
    os << "lower_bound map: " << first_it->first << std::endl;
    os << "lower_bound ft_map: " << second_it->first << std::endl;

    if (first_it->first != second_it->first)
    {
        *out << os.str();
        return (false);
    }

    *out << os.str();
    return (true);
}

bool map_check_upper_bound(std::ofstream *out)
{
    std::stringstream os;
    os << BLUE << "\n🤖 map_check_upper_bound : \n" << RESET;
    os << "\nCreaty map(first second)\n";
    std::map<int,std::string> first;
    ft::map<int,std::string> second;
    std::map<int,std::string>::iterator first_it;
    ft::map<int,std::string>::iterator second_it;

    first[1]="10";
    first[2]="30";
    first[3]="50";
    first[4]="70";
    first[5]="101";
    first[6]="301";
    first[7]="501";
    first[8]="701";

    second[1]="10";
    second[2]="30";
    second[3]="50";
    second[4]="70";
    second[5]="101";
    second[6]="301";
    second[7]="501";
    second[8]="701";

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#1 upper_bound(1)\n";
    first_it = first.upper_bound(1);
    second_it = second.upper_bound(1);
    os << "upper_bound map: " << first_it->first << std::endl;
    os << "upper_bound ft_map: " << second_it->first << std::endl;

    if (first_it->first != second_it->first)
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#2 upper_bound(2)\n";
    first_it = first.upper_bound(2);
    second_it = second.upper_bound(2);
    os << "upper_bound map: " << first_it->first << std::endl;
    os << "upper_bound ft_map: " << second_it->first << std::endl;

    if (first_it->first != second_it->first)
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#3 upper_bound(7)\n";
    first_it = first.upper_bound(7);
    second_it = second.upper_bound(7);
    os << "upper_bound map: " << first_it->first << std::endl;
    os << "upper_bound ft_map: " << second_it->first << std::endl;

    if (first_it->first != second_it->first)
    {
        *out << os.str();
        return (false);
    }

    *out << os.str();
    return (true);
}

bool map_check_equal_range(std::ofstream *out)
{
    std::stringstream os;
    os << BLUE << "\n🤖 map_check_equel_range: \n" << RESET;
    os << "\nCreaty map(first second)\n";
    std::map<int,std::string> first;
    ft::map<int,std::string> second;
    std::pair<std::map<int,std::string>::const_iterator, std::map<int,std::string>::const_iterator> first_it;
    std::pair<ft::map<int,std::string>::const_iterator, ft::map<int,std::string>::const_iterator> second_it;

    first[1]="10";
    first[2]="30";
    first[3]="50";
    first[4]="70";
    first[5]="101";
    first[6]="301";
    first[7]="501";
    first[8]="701";

    second[1]="10";
    second[2]="30";
    second[3]="50";
    second[4]="70";
    second[5]="101";
    second[6]="301";
    second[7]="501";
    second[8]="701";

    print_map(os, first, second);
    print_size_map(os, first, second);
    if (first.size() != second.size())
    {
        *out << os.str();
        return (false);
    }
    if (!check_map(first, second))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#1 equal_range(1)\n";
    first_it = first.equal_range(1);
    second_it = second.equal_range(1);
    os << "lower_bound map: " << first_it.first->first << std::endl;
    os << "lower_bound ft_map: " << second_it.first->first << std::endl;
    os << "upper_bound map: " << first_it.second->first << std::endl;
    os << "upper_bound ft_map: " << second_it.second->first << std::endl;
    if ((first_it.first->first != second_it.first->first) || (first_it.second->first != second_it.second->first))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#2 equal_range(2)\n";
    first_it = first.equal_range(2);
    second_it = second.equal_range(2);
    os << "lower_bound map: " << first_it.first->first << std::endl;
    os << "lower_bound ft_map: " << second_it.first->first << std::endl;
    os << "upper_bound map: " << first_it.second->first << std::endl;
    os << "upper_bound ft_map: " << second_it.second->first << std::endl;
    if ((first_it.first->first != second_it.first->first) || (first_it.second->first != second_it.second->first))
    {
        *out << os.str();
        return (false);
    }

    os << "\nTest#3 equal_range(7)\n";
    first_it = first.equal_range(7);
    second_it = second.equal_range(7);
    os << "lower_bound map: " << first_it.first->first << std::endl;
    os << "lower_bound ft_map: " << second_it.first->first << std::endl;
    os << "upper_bound map: " << first_it.second->first << std::endl;
    os << "upper_bound ft_map: " << second_it.second->first << std::endl;
    if ((first_it.first->first != second_it.first->first) || (first_it.second->first != second_it.second->first))
    {
        *out << os.str();
        return (false);
    }

    *out << os.str();
    return (true);
}

void map_run(std::ofstream *out)
{
    std::cout << BLUE << "\n[check constructor]: \t" << ((map_check_constructor(out)) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check operator=]: \t" << ((map_check_operator_equal(out)) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check iterator]: \t" << ((map_check_iterator(out)) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check empty]: \t\t" << ((map_check_empty(out)) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check size]: \t\t" << ((map_check_size(out)) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check max_size]: \t" << ((map_check_max_size(out)) ? GREEN"OK" : YELLOW"DEPEND") << std::endl;
    std::cout << BLUE << "[check operator[] ]: \t" << ((map_check_operator_crochet(out)) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check insert]: \t" << ((map_check_insert(out)) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check erase]: \t\t" << ((map_check_erase(out)) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check swap]: \t\t" << ((map_check_swap(out)) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check clear]: \t\t" << ((map_check_clear(out)) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check key_comp]: \t" << ((map_check_key_comp(out)) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check value_comp]: \t" << ((map_check_value_comp(out)) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check find]: \t\t" << ((map_check_find(out)) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check count]: \t\t" << ((map_check_count(out)) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check lower_bound]: \t" << ((map_check_lower_bound(out)) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check upper_bound]: \t" << ((map_check_upper_bound(out)) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << BLUE << "[check equal_range]: \t" << ((map_check_equal_range(out)) ? GREEN"OK" : RED"FAIL") << std::endl;
    std::cout << RESET << std::endl;
}

#endif
