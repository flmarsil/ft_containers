#ifndef LIST_TEST_TEMPLATE_HPP
#define LIST_TEST_TEMPLATE_HPP

#include "../main.hpp"

/*** LOGS PRINT ***/
template <typename T>
void print_list(std::stringstream& os, std::list<T>& list, ft::list<T>& ft_list)
{
    typename std::list<T>::iterator it = list.begin();
    typename std::list<T>::iterator ite = list.end();

    typename ft::list<T>::iterator ft_it = ft_list.begin();
    typename ft::list<T>::iterator ft_ite = ft_list.end();
    
    os << "list :\t\t\t";
    for (; it != ite ; ++it)
        os << *it << " ";
    os << std::endl;

  
    os << "ft_list :\t\t";
    for (; ft_it != ft_ite ; ++ft_it)
        os << *ft_it << " ";
    os << std::endl;

    typename std::list<T>::reverse_iterator r_it = list.rbegin();
    typename std::list<T>::reverse_iterator r_ite = list.rend();
    typename ft::list<T>::reverse_iterator ft_r_it = ft_list.rbegin();
    typename ft::list<T>::reverse_iterator ft_r_ite = ft_list.rend();

    os << "list (reverse) :\t";
    for (; r_it != r_ite ; ++r_it)
        os << *r_it << " ";
    os << std::endl;

    os << "ft_list (reverse) :\t";
    for (; ft_r_it != ft_r_ite ; ++ft_r_it)
        os << *ft_r_it << " ";
    os << std::endl;
}

template <typename T>
void print_size(std::stringstream& os, std::list<T>& list, ft::list<T>& ft_list)
{
    os << "list.size = \t\t" << list.size() << std::endl;
    os << "ft_list.size = \t\t" << ft_list.size() << std::endl;
}

/*** CHECK LIST ***/
template <typename T>
bool check_list(std::list<T>& list, ft::list<T>& ft_list)
{
    typename std::list<T>::iterator it = list.begin();
    typename std::list<T>::iterator ite = list.end();
    typename ft::list<T>::iterator ft_it = ft_list.begin();
    typename ft::list<T>::iterator ft_ite = ft_list.end();

    for (; it != ite && ft_it != ft_ite ; ++it, ++ft_it)
        if (*it != *ft_it)
            return (false);
    
    typename std::list<T>::reverse_iterator r_it = list.rbegin();
    typename std::list<T>::reverse_iterator r_ite = list.rend();
    typename ft::list<T>::reverse_iterator ft_r_it = ft_list.rbegin();
    typename ft::list<T>::reverse_iterator ft_r_ite = ft_list.rend();

    for (; r_it != r_ite && ft_r_it != ft_r_ite ; ++r_it, ++ft_r_it)
        if (*r_it != *ft_r_it)
            return (false);

    if (list.size() != ft_list.size())
        return (false);

    return (true);
}

template<typename T>
bool list_check_constructor (std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_constructor : \n" << RESET;
    os << "\n~ Empty constructor:\n\n";
    std::list<T>  list;
    ft::list<T>   ft_list;

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n~ Constructor (size_type n, const value_type& val) :\n\n";
    std::list<T>  list1(4, data_a);
    ft::list<T>   ft_list1(4, data_a);

    print_list(os, list1, ft_list1);
    print_size(os, list1, ft_list1);

    if (check_list(list1, ft_list1) == false)
    {
        *p_log << os.str();
        return (false);
    }

    typename std::list<T>::iterator it = list1.begin();
    typename std::list<T>::iterator ite = list1.end();
    typename ft::list<T>::iterator ft_it = ft_list1.begin();
    typename ft::list<T>::iterator ft_ite = ft_list1.end();

    os << "\n~ Constructor (iterator first, iterator last) :\n\n";
    std::list<T>  list2(it, ite);
    ft::list<T>   ft_list2(ft_it, ft_ite);

    print_list(os, list2, ft_list2);
    print_size(os, list2, ft_list2);

    if (check_list(list2, ft_list2) == false)
    {
        *p_log << os.str();
        return (false);
    }

    typename std::list<T>::const_iterator it1 = list2.begin();
    typename std::list<T>::const_iterator ite1 = list2.end();
    typename ft::list<T>::const_iterator ft_it1 = ft_list2.begin();
    typename ft::list<T>::const_iterator ft_ite1 = ft_list2.end();

    os << "\n~ Constructor (const_iterator first, const_iterator last) :\n\n";
    std::list<T>  list3(it1, ite1);
    ft::list<T>   ft_list3(ft_it1, ft_ite1);

    print_list(os, list3, ft_list3);
    print_size(os, list3, ft_list3);

    if (check_list(list3, ft_list3) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n~ Constructor (const list& x) :\n\n";
    std::list<T>  list4(list1);
    ft::list<T>   ft_list4(ft_list1);
    print_list(os, list4, ft_list4);
    print_size(os, list4, ft_list4);

    if (check_list(list4, ft_list4) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_a;
    (void)data_b;
    *p_log << os.str();
    return (true);
}

/*** CHECK OPERATOR ***/
template <typename T>
bool list_check_operator(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_operator : \n" << RESET;
    os << "\n~ list& operator = (const list& assignObj); :\n\n";

    std::list<T> list(8, data_a);
    ft::list<T> ft_list(8, data_a);
    std::list<T> list2(6, data_b);
    ft::list<T> ft_list2(6, data_b);

    os << "\nBefore use operator overloading:\n";
    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }
    
    os << "\nBefore use operator overloading :\n";
    print_list(os, list2, ft_list2);
    print_size(os, list2, ft_list2);

    if (check_list(list2, ft_list2) == false)
    {
        *p_log << os.str();
        return (false);
    }

    list = list2;
    ft_list = ft_list2;

    os << "\nAfter use operator overloading :\n";
    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nAfter use operator overloading :\n";
    print_list(os, list2, ft_list2);
    print_size(os, list2, ft_list2);


    if (check_list(list2, ft_list2) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_a;
    (void)data_b;
    *p_log << os.str();
    return (true);
}

/*** BEGIN ***/
template <typename T>
bool list_check_begin(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_begin : \n" << RESET;
    os << "\n~ x.begin() :\n\n";

    std::list<T> list(5, data_a);
    ft::list<T> ft_list(5, data_a);

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    std::list<T> list2(10, data_b);
    ft::list<T> ft_list2(10, data_b);

    print_list(os, list2, ft_list2);
    print_size(os, list2, ft_list2);

    if (check_list(list2, ft_list2) == false)
    {
        *p_log << os.str();
        return (false);
    }

    typename std::list<T>::iterator it = list.begin();
    typename std::list<T>::iterator ite = list.end();
    typename ft::list<T>::iterator ft_it = ft_list.begin();
    typename ft::list<T>::iterator ft_ite = ft_list.end();

    typename std::list<T>::const_iterator it1 = list.begin();
    typename std::list<T>::const_iterator ite1 = list.end();
    typename ft::list<T>::const_iterator ft_it1 = ft_list.begin();
    typename ft::list<T>::const_iterator ft_ite1 = ft_list.end();

    typename std::list<T>::iterator it2 = list2.begin();
    typename std::list<T>::iterator ite2 = list2.end();
    typename ft::list<T>::iterator ft_it2 = ft_list2.begin();
    typename ft::list<T>::iterator ft_ite2 = ft_list2.end();

    typename std::list<T>::const_iterator it3 = list2.begin();
    typename std::list<T>::const_iterator ite3 = list2.end();
    typename ft::list<T>::const_iterator ft_it3 = ft_list2.begin();
    typename ft::list<T>::const_iterator ft_ite3 = ft_list2.end();

    if (!check_iterators<typename std::list<T>::iterator, typename ft::list<T>::iterator>(it, ite, ft_it, ft_ite) ||
    !check_iterators<typename std::list<T>::const_iterator, typename ft::list<T>::const_iterator>(it1, ite1, ft_it1, ft_ite1) ||
    !check_iterators<typename std::list<T>::iterator, typename ft::list<T>::iterator>(it2, ite2, ft_it2, ft_ite2) ||
    !check_iterators<typename std::list<T>::const_iterator, typename ft::list<T>::const_iterator>(it3, ite3, ft_it3, ft_ite3))
    {
        *p_log << os.str();
        return (false);
    }

    *p_log << os.str();
    return (true);
}

/*** RBEGIN ***/
template <typename T>
bool list_check_rbegin(std::ofstream* p_log, T data_a, T data_b)
{
    
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_rbegin : \n" << RESET;
    os << "\n~ x.rbegin() :\n\n";

    std::list<T> list(5, data_a);
    ft::list<T> ft_list(5, data_a);

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n";
    std::list<T> list2(10, data_b);
    ft::list<T> ft_list2(10, data_b);

    print_list(os, list2, ft_list2);
    print_size(os, list2, ft_list2);

    if (check_list(list2, ft_list2) == false)
    {
        *p_log << os.str();
        return (false);
    }

    typename std::list<T>::reverse_iterator it = list.rbegin();
    typename std::list<T>::reverse_iterator ite = list.rend();

    typename std::list<T>::reverse_iterator it2 = list2.rbegin();
    typename std::list<T>::reverse_iterator ite2 = list2.rend();

    typename ft::list<T>::reverse_iterator ft_it = ft_list.rbegin();
    typename ft::list<T>::reverse_iterator ft_ite = ft_list.rend();

    typename ft::list<T>::reverse_iterator ft_it2 = ft_list2.rbegin();
    typename ft::list<T>::reverse_iterator ft_ite2 = ft_list2.rend();

    typename std::list<T>::const_reverse_iterator it1 = list.rbegin();
    typename std::list<T>::const_reverse_iterator ite1 = list.rend();

    typename std::list<T>::const_reverse_iterator it3 = list2.rbegin();
    typename std::list<T>::const_reverse_iterator ite3 = list2.rend();

    typename ft::list<T>::const_reverse_iterator ft_it1 = ft_list.crbegin();
    typename ft::list<T>::const_reverse_iterator ft_ite1 = ft_list.crend();

    typename ft::list<T>::const_reverse_iterator ft_it3 = ft_list2.crbegin();
    typename ft::list<T>::const_reverse_iterator ft_ite3 = ft_list2.crend();

    if (!check_iterators<typename std::list<T>::reverse_iterator, typename ft::list<T>::reverse_iterator>(it, ite, ft_it, ft_ite) ||
    !check_iterators<typename std::list<T>::const_reverse_iterator, typename ft::list<T>::const_reverse_iterator>(it1, ite1, ft_it1, ft_ite1) ||
    !check_iterators<typename std::list<T>::reverse_iterator, typename ft::list<T>::reverse_iterator>(it2, ite2, ft_it2, ft_ite2) ||
    !check_iterators<typename std::list<T>::const_reverse_iterator, typename ft::list<T>::const_reverse_iterator>(it3, ite3, ft_it3, ft_ite3))
    {
        *p_log << os.str();
        return (false);
    }

    *p_log << os.str();
    return (true);
}


/*** MAXSIZE ***/
template <typename T>
bool list_check_maxsize(std::ofstream* p_log, T data_a, T data_b)
{
    
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_maxsize : \n\n" << RESET;
    os << "~ size_type max_size() const\n\n";

    std::list<T> list;
    ft::list<T> ft_list;

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nlist.maxsize = \t" << list.max_size() << std::endl;
    os << "ft_list.maxsize = \t" << ft_list.max_size() << std::endl;

    if (list.max_size() != ft_list.max_size())
    {
        *p_log << os.str();
        return (false);
    }

    list.assign(5, data_a);
    ft_list.assign(5, data_a);

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

  
    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nlist.maxsize = \t" << list.max_size() << std::endl;
    os << "ft_list.maxsize = \t" << ft_list.max_size() << std::endl;
    os << "\n";
    
    if (list.max_size() != ft_list.max_size())
    {
        *p_log << os.str();
        return (false);
    }

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);
    
    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;

    *p_log << os.str();
    return (true);
}


/*** RESIZE ***/
template <typename T>
bool list_check_resize(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_resize : \n\n" << RESET;
    os << "~ void resize (size_type n, value_type val = value_type());\n";
 
    os << "\nBefore resize lists, empty:\n\n" << std::endl;

    std::list<T> list;
    ft::list<T> ft_list;

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nAfter resize lists 5 :\n" << std::endl;

    list.resize(5, data_a);
    ft_list.resize(5, data_a);

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nAfter resize lists 2 :\n" << std::endl;
    list.resize(2, data_a);
    ft_list.resize(2, data_a);

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;
    (void)data_a;

    *p_log << os.str();
    return (true);
}

/*** EMPTY ***/
template <typename T>
bool list_check_empty(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_empty: \n\n" << RESET;
    os << "~ void empty();\n";
 
    os << "\nBefore, with empty list:\n\n" << std::endl;

    std::list<T> list;
    ft::list<T> ft_list;

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nEmpty list : \t\t" << list.empty() << std::endl;
    os << "Empty ft_list : \t" << ft_list.empty() << std::endl;
    if ((list.empty()) != (ft_list.empty()))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nAfter assign :\n\n" << std::endl;

    list.assign(6, data_a);
    ft_list.assign(6, data_a);

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nEmpty list : \t\t" << list.empty() << std::endl;
    os << "Empty ft_list : \t" << ft_list.empty() << std::endl;
    if ((list.empty()) != (ft_list.empty()))
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;

    *p_log << os.str();
    return (true);
}

/*** FRONT ***/
template <typename T>
bool list_check_front(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_front : \n\n" << RESET;
    os << "~ reference front ();\n\n";

    std::list<T> list(10, data_a);
    ft::list<T> ft_list(10, data_a);

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nlist : \t\t" << list.front() << std::endl;
    os << "ft_list : \t\t" << ft_list.front() << std::endl;

    if ((list.front()) != (ft_list.front()))
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;
    (void)data_a;

    *p_log << os.str();
    return (true);
}

/*** BACK ***/
template <typename T>
bool list_check_back(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_back : \n\n" << RESET;
    os << "~ reference back ();\n\n";

    std::list<T> list(10, data_a);
    ft::list<T> ft_list(10, data_a);
    list.push_back(data_b);
    ft_list.push_back(data_b);

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nlist : \t\t" << list.back() << std::endl;
    os << "ft_list : \t\t" << ft_list.back() << std::endl;

    if ((list.back()) != (ft_list.back()))
    {
        *p_log << os.str();
        return (false);
    }

    *p_log << os.str();
    return (true);
}


/*** ASSIGN ***/
template <typename T>
bool list_check_assign(std::ofstream* p_log, T data_a, T data_b)
{
    
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_assign : \n\n" << RESET;
    os << "~ void assign (iterator first, iterator last);\n\n";

    os << "Before, with empty list :\n\n";
    std::list<T> list;
    ft::list<T> ft_list;

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nAssign 5 with data_a to the same list :\n\n";

    list.assign(5, data_a);
    ft_list.assign(5, data_a);

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;

    *p_log << os.str();
    return (true);
}


/*** PUSH_BACK ***/
template <typename T>
bool list_check_push_back(std::ofstream* p_log, T data_a, T data_b)
{
    
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_push_back : \n\n" << RESET;
    os << "~ void push_back (const value_type& val);\n\n";

    os << "Before:\n\n";
    std::list<T> list(3, data_b);
    ft::list<T> ft_list(3, data_b);

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nAfter, push_back(data_a) x 4 :\n\n";

    for (int i = 0 ; i < 4 ; i++)
    {
        list.push_back(data_a);
        ft_list.push_back(data_a);
    }
    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;

    *p_log << os.str();
    return (true);
}

/*** POP_BACK ***/
template <typename T>
bool list_check_pop_back(std::ofstream* p_log, T data_a, T data_b)
{
    
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_pop_back : \n\n" << RESET;
    os << "~ void pop_back();\n\n";

    os << "Before, with 4 x data_a :\n\n";
    std::list<T> list(4, data_a);
    ft::list<T> ft_list(4, data_a);

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nAfter, pop_back() x 2 :\n\n";

    for (int i = 0 ; i < 2 ; i++)
    {
        list.pop_back();
        ft_list.pop_back();
    }
    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;

    *p_log << os.str();
    return (true);
}

/*** CLEAR ***/
template <typename T>
bool list_check_clear(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_clear : \n\n" << RESET;
    os << "~ void clear();\n\n";

    os << "Before, with 4 x data_a :\n\n";
    std::list<T> list(4, data_a);
    ft::list<T> ft_list(4, data_a);

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nAfter use clear() :\n\n";

    list.clear();
    ft_list.clear();

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;

    *p_log << os.str();
    return (true);
}

/*** INSERT ***/
template <typename T>
bool list_check_insert(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_insert : \n\n" << RESET;

    os << "Before, with 4 x data_a :\n\n";
    std::list<T> list(4, data_a);
    ft::list<T> ft_list(4, data_a);

    for (size_t i = 0; i < 4 ; i++)
    {
        list.push_back(data_b);
        ft_list.push_back(data_b);
    }

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n~ void insert (iterator position, size_type n, const value_type& val);\n\n";
    
    os << "Insert data_a value to begin and data_b to end\n\n";
    typename std::list<T>::iterator it = list.begin();
    typename ft::list<T>::iterator ft_it = ft_list.begin();
    list.insert(it, data_b);
    ft_list.insert(ft_it, data_b);

    typename std::list<T>::iterator ite = list.end();
    typename ft::list<T>::iterator ft_ite = ft_list.end();
    list.insert(ite, data_a);
    ft_list.insert(ft_ite, data_a);

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\n~ void insert (iterator position, size_type n, const value_type& val);\n\n";
    it = list.begin();
    ft_it = ft_list.begin();
    list.insert(it, 4, data_a);
    ft_list.insert(ft_it, 4, data_a);

    ite = list.begin();
    ft_ite = ft_list.begin();
    list.insert(it, 4, data_b);
    ft_list.insert(ft_it, 4, data_b);

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    std::list<T> list2(11, data_a);
    ft::list<T> ft_list2(11, data_a);

    it = list2.begin();
    ite = list2.end();
    ft_it = ft_list2.begin();
    ft_ite = ft_list2.end();

    os << "\nAfter run insert (iterator position, iterator first, iterator last)\n";
    list.insert(list.begin(), it, ite);
    ft_list.insert(ft_list.begin(), ft_it, ft_ite);


    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_a;
    (void)data_b;
    *p_log << os.str();
    return (true);
}

/*** ERASE ***/
template <typename T>
bool list_check_erase(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_erase : \n\n" << RESET;

    os << "Before, with 4 x data_a :\n\n";
    std::list<T> list(4, data_a);
    ft::list<T> ft_list(4, data_a);

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nInsert data_b x3 to begin :\n\n";
    typename std::list<T>::iterator it = list.begin();
    typename ft::list<T>::iterator ft_it = ft_list.begin();
    list.insert(it, 3, data_b);
    ft_list.insert(ft_it, 3, data_b);

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nNow erase data_b x1 :\n\n";
    typename std::list<T>::iterator it1 = list.begin();
    typename ft::list<T>::iterator ft_it1 = ft_list.begin();

    list.erase(++it1);
    ft_list.erase(++ft_it1);

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    typename std::list<T>::iterator it2 = list.begin();
    typename ft::list<T>::iterator ft_it2 = ft_list.begin();
    typename std::list<T>::iterator ite2 = list.end();
    typename ft::list<T>::iterator ft_ite2 = ft_list.end();
    
    os << "\nNow erase all the rest:\n\n";
    
    list.erase(it2, ite2);
    ft_list.erase(ft_it2, ft_ite2);

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_a;
    (void)data_b;
    *p_log << os.str();
    return (true);
}

/*** SWAP ***/
template <typename T>
bool list_check_swap(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_swap : \n\n" << RESET;

    os << "std::list 1 :\t\t";
    std::list<T> list(4, data_a);
    typename std::list<T>::iterator it = list.begin();
    typename std::list<T>::iterator ite = list.end();

    for (; it != ite ; ++it)
        os << *it << " ";
    os << std::endl;

    os << "std::list_swap :\t";
    std::list<T> list_swap(4, data_b);
    typename std::list<T>::iterator it_swap = list_swap.begin();
    typename std::list<T>::iterator ite_swap = list_swap.end();

    for (; it_swap != ite_swap ; ++it_swap)
        os << *it_swap << " ";
    os << std::endl;

    os << "\nAfter swap :\n\n";
    list.swap(list_swap);

    typename std::list<T>::iterator it1 = list.begin();
    typename std::list<T>::iterator ite1 = list.end();
    os << "std::list 1 :\t\t";
    for (; it1 != ite1 ; ++it1)
        os << *it1 << " ";
    os << std::endl;

    typename std::list<T>::iterator it_swap1 = list_swap.begin();
    typename std::list<T>::iterator ite_swap1 = list_swap.end();
    os << "std::list_swap :\t";
    for (; it_swap1 != ite_swap1 ; ++it_swap1)
        os << *it_swap1 << " ";
    os << std::endl;


    os << "\n\nft::list 1 :\t\t";
    ft::list<T> ft_list(4, data_a);
    typename ft::list<T>::iterator ft_it = ft_list.begin();
    typename ft::list<T>::iterator ft_ite = ft_list.end();

    for (; ft_it != ft_ite ; ++ft_it)
        os << *ft_it << " ";
    os << std::endl;

    os << "ft::list_swap :\t\t";
    ft::list<T> ft_list_swap(4, data_b);
    typename ft::list<T>::iterator ft_it_swap = ft_list_swap.begin();
    typename ft::list<T>::iterator ft_ite_swap = ft_list_swap.end();
    for (; ft_it_swap != ft_ite_swap ; ++ft_it_swap)
        os << *ft_it_swap << " ";
    os << std::endl;

    os << "\nAfter swap :\n\n";
    ft_list.swap(ft_list_swap);

    typename ft::list<T>::iterator ft_it1 = ft_list.begin();
    typename ft::list<T>::iterator ft_ite1 = ft_list.end();

    os << "ft::list 1 :\t\t";
    for (; ft_it1 != ft_ite1 ; ++ft_it1)
        os << *ft_it1 << " ";
    os << std::endl;

    typename ft::list<T>::iterator ft_it_swap1 = ft_list_swap.begin();
    typename ft::list<T>::iterator ft_ite_swap1 = ft_list_swap.end();
    os << "ft::list_swap :\t\t";
    for (; ft_it_swap1 != ft_ite_swap1 ; ++ft_it_swap1)
        os << *ft_it_swap1 << " ";
    os << std::endl;

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    if (check_list(list_swap, ft_list_swap) == false)
    {
        *p_log << os.str();
        return (false);
    }

    *p_log << os.str();
    return (true);
}

/*** PUSH FRONT***/
template <typename T>
bool list_check_push_front(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_push_front : \n\n" << RESET;
    os << "~ void push_front (const value_type& val);\n\n";

    std::list<T> list(3, data_a);
    ft::list<T> ft_list(3, data_a);
    list.push_front(data_b);
    ft_list.push_front(data_b);

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nlist : \t\t\t" << list.front() << std::endl;
    os << "ft_list : \t\t" << ft_list.front() << std::endl;

    if ((list.front()) != (ft_list.front()))
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;
    *p_log << os.str();
    return (true);
}

/*** POP_FRONT ***/
template <typename T>
bool list_check_pop_front(std::ofstream* p_log, T data_a, T data_b)
{
    
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_pop_front : \n\n" << RESET;
    os << "~ void pop_front();\n\n";

    os << "Before, with 4 x data_a :\n\n";
    std::list<T> list(4, data_a);
    ft::list<T> ft_list(4, data_a);

    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nAfter, pop_front() x 2 :\n\n";

    for (int i = 0 ; i < 2 ; i++)
    {
        list.pop_front();
        ft_list.pop_front();
    }
    print_list(os, list, ft_list);
    print_size(os, list, ft_list);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    (void)data_b;

    *p_log << os.str();
    return (true);
}

/*** Splice ***/
template <typename T>
bool list_check_splice(std::ofstream* p_log, T data_a, T data_b)
{
    
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_splice : \n\n" << RESET;
    os << "~ void splice (iterator position, list& x);\n\n";

    os << "Before, splice:\n\n";

    std::list<T> list(4, data_a);
    ft::list<T> ft_list(4, data_a);
    os << "Liste 0:\n\n";
    print_list(os, list, ft_list);
    print_size(os, list, ft_list);
    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    std::list<T> list1(4, data_b);
    ft::list<T> ft_list1(4, data_b);
    os << "\nListe 1:\n\n";
    print_list(os, list1, ft_list1);
    print_size(os, list1, ft_list1);
    if (check_list(list1, ft_list1) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "After, splice (iterator position, list& x) :\n\n";
    list.splice(++(list.begin()), list1);
    ft_list.splice(++(ft_list.begin()), ft_list1);

    os << "Liste 0:\n\n";
    print_list(os, list, ft_list);
    print_size(os, list, ft_list);
    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nListe 1:\n\n";
    print_list(os, list1, ft_list1);
    print_size(os, list1, ft_list1);
    if (check_list(list1, ft_list1) == false)
    {
        *p_log << os.str();
        return (false);
    }

    std::list<T> list2(6, data_a);
    ft::list<T> ft_list2(6, data_a);
    os << "\nAfter, splice (iterator position, list& x, iterator i); \n";
    list.splice(++(list.begin()), list2, --(list2.end()));
    ft_list.splice(++(ft_list.begin()), ft_list2, --(ft_list2.end()));

    os << "\nListe 2:\n\n";
    print_list(os, list2, ft_list2);
    print_size(os, list2, ft_list2);

    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }
    if (check_list(list2, ft_list2) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nListe 3 :\n\n";
    std::list<T> list3(6, data_b);
    ft::list<T> ft_list3(6, data_b);
    print_list(os, list3, ft_list3);
    print_size(os, list3, ft_list3);
    if (check_list(list3, ft_list3) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nAfter splice (iterator position, list& x, iterator first, iterator last); \n";    list.splice(++(list.begin()), list2, --(list2.end()));
    list1.splice(list1.begin(), list3, list3.begin(), --(list3.end()));
    ft_list1.splice(ft_list1.begin(), ft_list3, ft_list3.begin(), --(ft_list3.end()));

    os << "\nListe 1:\n\n";
    print_list(os, list1, ft_list1);
    print_size(os, list1, ft_list1);
    if (check_list(list1, ft_list1) == false)
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nListe 3:\n\n";
    print_list(os, list3, ft_list3);
    print_size(os, list3, ft_list3);
    if (check_list(list3, ft_list3) == false)
    {
        *p_log << os.str();
        return (false);
    }

    *p_log << os.str();
    return (true);
}


/*** REMOVE ***/
template <typename T>
bool list_check_remove(std::ofstream* p_log, T data_a, T data_b)
{
    
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_remove : \n\n" << RESET;
    os << "~ void remove (const value_type& val);\n\n";

    os << "Before, remove:\n\n";

    std::list<T> list(4, data_a);
    ft::list<T> ft_list(4, data_a);
    list.push_front(data_b);
    list.push_back(data_b);
    ft_list.push_front(data_b);
    ft_list.push_back(data_b);

    os << "Liste before remove:\n\n";
    print_list(os, list, ft_list);
    print_size(os, list, ft_list);
    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    list.remove(data_b);
    ft_list.remove(data_b);

    os << "\nListe after remove data_b:\n\n";
    print_list(os, list, ft_list);
    print_size(os, list, ft_list);
    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    *p_log << os.str();
    return (true);
}

/*** REMOVE IF ***/
template <typename T>
bool list_check_remove_if(std::ofstream* p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_remove_if : \n\n" << RESET;
    os << "~ void remove_if (Predicate pred);\n\n";

    os << "Before, remove_if:\n\n";

    std::list<T> list(4, data_a);
    ft::list<T> ft_list(4, data_a);
    list.push_front(data_b);
    list.push_back(data_b);
    ft_list.push_front(data_b);
    ft_list.push_back(data_b);

    os << "Liste before remove:\n\n";
    print_list(os, list, ft_list);
    print_size(os, list, ft_list);
    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    list.remove_if(Predict<T>);
    ft_list.remove_if(Predict<T>);

    os << "\nListe after remove data_b:\n\n";
    print_list(os, list, ft_list);
    print_size(os, list, ft_list);
    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }
    
    *p_log << os.str();
    return (true);
}

/*** UNIQUE ***/
template <typename T>
bool list_check_unique(std::ofstream* p_log, T data_a, T data_b)
{ 
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_unique : \n\n" << RESET;
    os << "~ void unique();\n\n";

    os << "Before, unique:\n\n";

    std::list<T> list(4, data_a);
    ft::list<T> ft_list(4, data_a);
    list.insert(list.begin(), 5, data_b);
    ft_list.insert(ft_list.begin(), 5, data_b);
    list.insert(list.end(), 5, data_b);
    ft_list.insert(ft_list.end(), 5, data_b);

    os << "Liste before remove:\n\n";
    print_list(os, list, ft_list);
    print_size(os, list, ft_list);
    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }

    list.unique();
    ft_list.unique();

    os << "\nListe after remove data_b:\n\n";
    print_list(os, list, ft_list);
    print_size(os, list, ft_list);
    if (check_list(list, ft_list) == false)
    {
        *p_log << os.str();
        return (false);
    }
    
    *p_log << os.str();
    return (true);
}

template<typename T>
bool list_check_merge(std::ofstream *p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_merge : \n\n" << RESET;
    os << "\nlist #1 list ft_list \n";
    std::list<T> list1(5, data_a);
    ft::list<T> ft_list1(5, data_a);
    print_list(os, list1, ft_list1);
    print_size(os, list1, ft_list1);
    if (!check_list(list1, ft_list1))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nlist #2 list ft_list \n";
    std::list<T> list2(4, data_b);
    ft::list<T> ft_list2(4, data_b);
    print_list(os, list2, ft_list2);
    print_size(os, list2, ft_list2);
    if (!check_list(list2, ft_list2))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nAfter merge (list& x) \n";
    list1.merge(list2);
    ft_list1.merge(ft_list2);

    os << "\nlist #1 list ft_list \n";
    print_list(os, list1, ft_list1);
    print_size(os, list1, ft_list1);
    if (!check_list(list1, ft_list1))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nlist #2 list ft_list \n";
    print_list(os, list2, ft_list2);
    print_size(os, list2, ft_list2);
    if (!check_list(list2, ft_list2))
    {
        *p_log << os.str();
        return (false);
    }

    list2.assign(4, data_b);
    ft_list2.assign(4, data_b);
    os << "\n After assign list #2 list ft_list \n";
    print_list(os, list2, ft_list2);
    print_size(os, list2, ft_list2);
    if (!check_list(list2, ft_list2))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nAfter merge (list& x, Compare comp) \n";

    list1.merge(list2, Compare_my<T>);
    ft_list1.merge(ft_list2, Compare_my<T>);

    os << "\nlist #1 list ft_list \n";
    print_list(os, list1, ft_list1);
    print_size(os, list1, ft_list1);
    if (!check_list(list1, ft_list1))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nlist #2 list ft_list \n";
    print_list(os, list2, ft_list2);
    print_size(os, list2, ft_list2);
    if (!check_list(list2, ft_list2))
    {
        *p_log << os.str();
        return (false);
    }

    *p_log << os.str();
    return (true);
}


template<typename T>
bool list_check_sort(std::ofstream *p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_sort : \n\n" << RESET;
    os << "~ void sort ();\n\n";

    os << "Before, sort:\n\n";
    std::list<T> list;
    ft::list<T> ft_list;

    for (size_t i = 0; i < 4; i++)
    {
        if (i % 2 == 0)
        {
            list.push_back(data_b);
            ft_list.push_back(data_b);
            list.push_back(data_b);
            ft_list.push_back(data_b);
        }
        else
        {
            list.push_back(data_a);
            ft_list.push_back(data_a);
        }
    }
    print_list(os, list, ft_list);
    print_size(os, list, ft_list);
    if (!check_list(list, ft_list))
    {
        *p_log << os.str();
        return (false);
    }

    list.sort();
    ft_list.sort();

    os << "\nAfter sort () \n";
    print_list(os, list, ft_list);
    print_size(os, list, ft_list);
    if (!check_list(list, ft_list))
    {
        *p_log << os.str();
        return (false);
    }

    os << "\nAdd and push himself  \n";
    for (size_t i = 0; i < 4; i++)
    {
        if (i % 2 == 0)
        {
            list.push_back(data_b);
            ft_list.push_back(data_b);
            list.push_back(data_b);
            ft_list.push_back(data_b);
        }
        else
        {
            list.push_back(data_a);
            ft_list.push_back(data_a);
        }
    }
    print_list(os, list, ft_list);
    print_size(os, list, ft_list);
    if (!check_list(list, ft_list))
    {
        *p_log << os.str();
        return (false);
    }

    list.sort(Compare_my<T>);
    ft_list.sort(Compare_my<T>);

    os << "\nAfter sort (Compare comp) \n";
    print_list(os, list, ft_list);
    print_size(os, list, ft_list);
    if (!check_list(list, ft_list))
    {
        *p_log << os.str();
        return (false);
    }

    *p_log << os.str();
    return (true);
}

template<typename T>
bool list_check_reverse(std::ofstream *p_log, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_reverse : \n\n" << RESET;
    os << "~ void reverse ();\n\n";

    os << "Before, reverse :\n\n";

    os << "\nlist #1 list ft_list \n";
    std::list<T> list;
    ft::list<T> ft_list;

    for (size_t i = 0; i < 11; i++)
    {
        if (i % 2 == 0)
        {
            list.push_back(data_b);
            ft_list.push_back(data_b);
            list.push_back(data_b);
            ft_list.push_back(data_b);
        }
        else
        {
            list.push_back(data_a);
            ft_list.push_back(data_a);
        }
    }
    print_list(os, list, ft_list);
    print_size(os, list, ft_list);
    if (!check_list(list, ft_list))
    {
        *p_log << os.str();
        return (false);
    }

    list.reverse();
    ft_list.reverse();

    os << "\nAfter reverse \n";
    print_list(os, list, ft_list);
    print_size(os, list, ft_list);
    if (!check_list(list, ft_list))
    {
        *p_log << os.str();
        return (false);
    }

    list.reverse();
    ft_list.reverse();

    os << "\nAfter unique \n";
    print_list(os, list, ft_list);
    print_size(os, list, ft_list);
    if (!check_list(list, ft_list))
    {
        *p_log << os.str();
        return (false);
    }

    *p_log << os.str();
    return (true);
}

template<typename T>
bool list_check_relational(std::ofstream *out, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_relational: \n\n" << RESET;
    os << "\nlist #1 list ft_list \n";
    std::list<T> list1(5, data_a);
    ft::list<T> ft_list1(5, data_a);
    print_list(os, list1, ft_list1);
    print_size(os, list1, ft_list1);
    if (!check_list(list1, ft_list1))
    {
        *out << os.str();
        return (false);
    }

    os << "\nlist #2 list ft_list \n";
    std::list<T> list2(15, data_b);
    ft::list<T> ft_list2(15, data_b);
    print_list(os, list2, ft_list2);
    print_size(os, list2, ft_list2);
    if (!check_list(list2, ft_list2))
    {
        *out << os.str();
        return (false);
    }

    os << "\n[List]: After operator== " << (list1 == list2) << std::endl;
    os << "\n[ft_list]: After operator== " << (ft_list1 == ft_list2) << std::endl;
    if ((list1 == list2) != (ft_list1 == ft_list2))
    {
        *out << os.str();
        return (false);
    }

    os << "\n[List]: After operator!= " << (list1 != list2) << std::endl;
    os << "\n[ft_list]: After operator!= " << (ft_list1 != ft_list2) << std::endl;
    if ((list1 != list2) != (ft_list1 != ft_list2))
    {
        *out << os.str();
        return (false);
    }

    os << "\n[List]: After operator< " << (list1 < list2) << std::endl;
    os << "\n[ft_list]: After operator< " << (ft_list1 < ft_list2) << std::endl;
    if ((list1 < list2) != (ft_list1 < ft_list2))
    {
        *out << os.str();
        return (false);
    }

    os << "\n[List]: After operator<= " << (list1 <= list2) << std::endl;
    os << "\n[ft_list]: After operator<= " << (ft_list1 <= ft_list2) << std::endl;
    if ((list1 <= list2) != (ft_list1 <= ft_list2))
    {
        *out << os.str();
        return (false);
    }
    
    os << "\n[List]: After operator> " << (list1 > list2) << std::endl;
    os << "\n[ft_list]: After operator> " << (ft_list1 > ft_list2) << std::endl;
    if ((list1 > list2) != (ft_list1 > ft_list2))
    {
        *out << os.str();
        return (false);
    }

    os << "\n[List]: After operator>= " << (list1 >= list2) << std::endl;
    os << "\n[ft_list]: After operator>= " << (ft_list1 >= ft_list2) << std::endl;
    if ((list1 >= list2) != (ft_list1 >= ft_list2))
    {
        *out << os.str();
        return (false);
    }

    *out << os.str();
    return (true);
}

template<typename T>
bool list_check_swap_2(std::ofstream *out, T data_a, T data_b)
{
    std::stringstream os;
    os << BLUE << "\n🤖 list_check_swap2 : \n\n" << RESET;
    os << "\nlist #1 list ft_list \n";
    std::list<T> list1(5, data_a);
    ft::list<T> ft_list1(5, data_a);
    print_list(os, list1, ft_list1);
    print_size(os, list1, ft_list1);
    if (!check_list(list1, ft_list1))
    {
        *out << os.str();
        return (false);
    }

    os << "\nlist #2 list ft_list \n";
    std::list<T> list2(5, data_b);
    ft::list<T> ft_list2(5, data_b);
    print_list(os, list2, ft_list2);
    print_size(os, list2, ft_list2);
    if (!check_list(list2, ft_list2))
    {
        *out << os.str();
        return (false);
    }

    os << "\nAfter ft::swap \n";
    std::swap(list1, list2);
    ft::swap(ft_list1, ft_list2);

    os << "\nlist #1 list ft_list \n";
    print_list(os, list1, ft_list1);
    print_size(os, list1, ft_list1);
    if (!check_list(list1, ft_list1))
    {
        *out << os.str();
        return (false);
    }

    os << "\nlist #2 list ft_list \n";
    print_list(os, list2, ft_list2);
    print_size(os, list2, ft_list2);
    if (!check_list(list2, ft_list2))
    {
        *out << os.str();
        return (false);
    }

    *out << os.str();
    return (true);
}

/*** LAUNCHER ***/
template <typename T>
void list_run(std::ofstream* p_log, T (*f)())
{
    std::cout << BLUE << "\n[check constructor]: \t" << ((list_check_constructor<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check operator =]: \t" << ((list_check_operator<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;

    std::cout << BLUE << "\n[check begin - end]: \t" << ((list_check_begin<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check rbegin - rend]: \t" << ((list_check_rbegin<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check empty]: \t\t" << ((list_check_empty<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check maxisze]: \t" << ((list_check_maxsize<T>(p_log, f(), f())) ? GREEN"OK" : YELLOW"DEPEND") << RESET;
    std::cout << BLUE << "\n[check front]: \t\t" << ((list_check_front<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check back]: \t\t" << ((list_check_back<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check assign]: \t" << ((list_check_assign<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check push_back]: \t" << ((list_check_push_back<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check pop_back]: \t" << ((list_check_pop_back<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check push_front]: \t" << ((list_check_push_front<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check pop_front]: \t" << ((list_check_pop_front<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check insert]: \t" << ((list_check_insert<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check erase]: \t\t" << ((list_check_erase<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check swap]: \t\t" << ((list_check_swap<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check resize]: \t" << ((list_check_resize<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check clear]: \t\t" << ((list_check_clear<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check splice]: \t" << ((list_check_splice<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check remove]: \t" << ((list_check_remove<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check remove_if]: \t" << ((list_check_remove_if<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check unique]: \t" << ((list_check_unique<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check merge]: \t\t" << ((list_check_merge<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check sort]: \t\t" << ((list_check_sort<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check reverse]: \t" << ((list_check_reverse<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check relational]: \t" << ((list_check_relational<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << BLUE << "\n[check swap_2]: \t" << ((list_check_swap_2<T>(p_log, f(), f())) ? GREEN"OK" : RED"FAIL") << RESET;
    std::cout << "\n\n";
}

template <typename T>
void list_launcher(std::ofstream* p_log)
{
    if (typeid(T) == typeid(int))
        list_run<int>(p_log, int_generator);
    else if (typeid(T) == typeid(float))
        list_run<float>(p_log, float_generator);
    else if (typeid(T) == typeid(double))
        list_run<double>(p_log, double_generator);
    else if (typeid(T) == typeid(char))
        list_run<char>(p_log, char_generator);
    else if (typeid(T) == typeid(std::string))
        list_run<std::string>(p_log, string_generator);
}
#endif
